CREATE TRIGGER CambioStatoLibroPrenotazione AFTER INSERT ON Prenotazione 
FOR EACH ROW 
UPDATE Libro SET StatoPrestito = "Prenotato" WHERE Codice = NEW.CodiceLibro;

DELIMITER $
CREATE TRIGGER CambioStatoLibroConsegna AFTER INSERT ON Consegna
FOR EACH ROW
BEGIN
	DECLARE CodiceLibro INT;
	IF NEW.Tipo = "Affidamento" THEN
		SET CodiceLibro = (SELECT CodiceLibro FROM Prenotazione WHERE Codice = NEW.CodicePrenotazione);
		UPDATE Libro SET StatoPrestito = "Consegnato" WHERE Codice = CodiceLibro;
	ELSE IF NEW.Tipo = "Restituzione" THEN
		SET CodiceLibro = (SELECT CodiceLibro FROM Prenotazione WHERE Codice = NEW.CodicePrenotazione);
		UPDATE Libro SET StatoPrestito = "Disponibile" WHERE Codice = CodiceLibro;
	END IF;
END $
DELIMITER ;


DELIMITER $
CREATE TRIGGER SospensioneAccount AFTER INSERT ON Segnalazione
FOR EACH ROW
BEGIN
	DECLARE NumeroSegnalazioni INT;
    SET NumeroSegnalazioni = (SELECT COUNT(*) FROM Segnalazione WHERE CodiceUtilizzatore = NEW.CodiceUtilizzatore);
	IF (NumeroSegnalazioni = 3) THEN
		UPDATE Utente SET StatoAccount = "Sospeso" WHERE Codice = NEW.CodiceUtilizzatore;
	END IF;
END $
DELIMITER ;


