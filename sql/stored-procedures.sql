DELIMITER $
CREATE PROCEDURE VISUALIZZA_UTENTE (EmailIN VARCHAR(30))
	BEGIN
		SELECT * FROM Utente WHERE Email = EmailIN;
    END $ 
DELIMITER ;

CREATE VIEW NUMERO_POSTI_LETTURA (Biblioteca, PostiLettura) AS
	SELECT B.Nome, COUNT(P.Numero) FROM Biblioteca B LEFT JOIN PostoLettura P ON P.NomeBiblioteca = B.Nome
	GROUP BY B.Nome;

CREATE VIEW NUMERO_EBOOK (Biblioteca, Ebook) AS
	SELECT B.Nome, COUNT(L.Codice) FROM Biblioteca B LEFT JOIN Libro L ON L.NomeBiblioteca = B.Nome AND L.Tipo = "Ebook"
	GROUP BY B.Nome;

CREATE VIEW NUMERO_CARTACEI (Biblioteca, Cartacei) AS
	SELECT B.Nome, COUNT(L.Codice) FROM Biblioteca B LEFT JOIN Libro L ON L.NomeBiblioteca = B.Nome AND L.Tipo = "Cartaceo"
	GROUP BY B.Nome;


DELIMITER $
CREATE PROCEDURE VISUALIZZA_BIBLIOTECHE (NomeIN VARCHAR(50))
BEGIN
	SELECT B.Nome, B.Indirizzo, N1.PostiLettura, N2.Ebook, N3.Cartacei FROM Biblioteca B, NUMERO_POSTI_LETTURA N1, NUMERO_EBOOK N2, NUMERO_CARTACEI N3
	WHERE B.Nome = N1.Biblioteca AND B.Nome = N2.Biblioteca AND B.Nome = N3.Biblioteca AND (NomeIN LIKE "" OR B.Nome LIKE CONCAT('%', NomeIN, '%'));
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_SEGNALAZIONI_ADMIN (CodiceAmministratoreIN INT)
BEGIN
	SELECT S.Dataa , U.Codice , S.Testo , U.Nome , U.Cognome 
	FROM Segnalazione S JOIN Utente U ON(S.CodiceUtilizzatore = U.Codice)
	WHERE CodiceAmministratore  = CodiceAmministratoreIN;
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_MESSAGGI_ADMIN (CodiceAmministratoreIN INT)
BEGIN
	SELECT U.Codice , M.Testo , M.Dataa  , U.Nome , U.Cognome , M.Titolo 
	FROM Messaggio M JOIN Utente U ON (M.CodiceUtilizzatore = U.Codice )
	WHERE M.CodiceAmministratore = CodiceAmministratoreIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_SEGNALAZIONI_UTENTE (CodiceUtilizzatoreIn INT)
BEGIN
	SELECT * FROM Segnalazione S, Utente U
	WHERE S.CodiceUtilizzatore = CodiceUtilizzatoreIn AND S.CodiceAmministratore = U.Codice;
END
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_MESSAGGI_UTENTE (CodiceUtilizzatoreIn INT)
BEGIN
	SELECT * FROM Messaggio M, Utente U
	WHERE M.CodiceUtilizzatore = CodiceUtilizzatoreIn AND M.CodiceAmministratore = U.Codice;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_BIBLIOTECA (NomeIN VARCHAR(50))
BEGIN
	SELECT B.Nome, B.Indirizzo, B.SitoWeb, B.NoteStoriche, B.Latitudine, B.Longitudine, N1.PostiLettura, N2.Ebook, N3.Cartacei FROM Biblioteca B, NUMERO_POSTI_LETTURA N1, NUMERO_EBOOK N2, NUMERO_CARTACEI N3
	WHERE B.Nome = NomeIN AND B.Nome = N2.Biblioteca AND B.Nome = N3.Biblioteca AND B.Nome = N1.Biblioteca;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_RECAPITI (NomeIN VARCHAR(50))
BEGIN
	SELECT Numero FROM Recapito WHERE NomeBiblioteca = NomeIN;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE VISUALIZZA_IMMAGINI_BIBLIOTECA (NomeIN VARCHAR(50))
BEGIN
	SELECT * FROM Immagine WHERE NomeBiblioteca = NomeIN;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE VISUALIZZA_POSTI_LETTURA (NomeIN VARCHAR(50), DataIN DATE, OrarioInizioIN TIME, OrarioFineIN TIME)
BEGIN
	SELECT L.Numero, L.PresaRete, L.PresaCorrente, (CASE WHEN COUNT(P.Codice)>0 THEN 1 ELSE 0 END) AS Prenotato
	FROM PostoLettura L LEFT JOIN PrenotazionePostoLettura P ON (L.Numero = P.NumeroPostoLettura AND L.NomeBiblioteca = P.NomeBiblioteca AND P.Data = DataIN AND P.OraFine > OrarioInizioIN AND P.OraInizio < OrarioFineIN)
	WHERE L.NomeBiblioteca = NomeIN GROUP BY L.Numero;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE FILTRA_PRENOTAZIONE(DataInizioIN DATE, DataFineIN DATE, CodLibroIN INT, CodUtenteIN INT, NomeBibliotecaIN VARCHAR(50))
BEGIN
SELECT P.Codice, P.DataInizio, P.DataFine, L.StatoPrestito, L.Titolo, U.Nome AS NomeU, U.Cognome AS CognomeU , U.Codice AS CodiceU
FROM Utente U, Prenotazione P, Libro L
WHERE L.NomeBiblioteca = NomeBibliotecaIN AND ((CodUtenteIN = 0) OR (U.Codice = CodUtenteIN)) AND ((CodLibroIN = 0) OR (P.CodiceLibro = CodLibroIN)) AND U.Codice = P.CodiceUtilizzatore AND P.CodiceLibro = L.Codice
AND DataInizioIN < P.DataInizio AND DataFineIN > P.DataFine;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE FILTRA_LIBRI(CodLibroIN INT, TitoloIN VARCHAR(100), Autore VARCHAR(300), AnnoPubblicazioneIN INT, TipoLibroIN ENUM("Cartaceo", "Ebook"), BibliotecaIN VARCHAR(50))
BEGIN
SELECT L.* FROM Libro L 
WHERE (BibliotecaIN LIKE "" OR BibliotecaIN LIKE L.NomeBiblioteca) AND (TitoloIN LIKE "" OR TitoloIN LIKE)

P.Codice, P.DataInizio, P.DataFine, L.Titolo, U.Nome AS NomeU, U.Cognome AS CognomeU FROM 
Utente U, Prenotazione P, Libro L
WHERE ((CodUtenteIN = 0) OR (U.Codice = CodUtenteIN)) AND ((CodLibroIN = 0) OR (P.CodiceLibro = CodLibroIN)) AND U.Codice = P.CodiceUtilizzatore AND P.CodiceLibro = L.Codice
AND DataInizioIN < P.DataInizio AND DataFineIN > P.DataFine;
END $
DELIMITER ;




DELIMITER $
CREATE PROCEDURE LISTA_AUTORI ()
BEGIN
	SELECT * FROM Autore;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE VISUALIZZA_CATALOGO(NomeBiblio VARCHAR(50))
BEGIN
	SELECT * FROM Libro WHERE NomeBiblioteca = NomeBiblio;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_LIBRI_Gen()
BEGIN
	SELECT * FROM Libro;
END $
DELIMITER ;


### Operazioni SOLO utilizzatori

DELIMITER $
CREATE PROCEDURE REGISTRA_UTILIZZATORE(NomeIN VARCHAR(30), CognomeIN VARCHAR(30), EmailIN VARCHAR(30), PasswordIN VARCHAR(60), DataNascitaIN DATE, LuogoNascitaIN VARCHAR(30), TelefonoIN VARCHAR(15), ProfessioneIN VARCHAR(30), IndirizzoIN VARCHAR(50), CittaIN VARCHAR(30), CAPIN CHAR(5))
BEGIN
	IF (EXISTS (SELECT * FROM Utente WHERE Email = EmailIN)) THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Utente già presente'; 
	ELSE
		INSERT INTO Utente (Nome, Cognome, Email, Password, DataNascita, LuogoNascita, Telefono, TipoUtente, StatoAccount, Professione, DataCreazione, Indirizzo, Citta, CAP) VALUES
    	(NomeIN, CognomeIN, EmailIN, PasswordIN, DataNascitaIN, LuogoNascitaIN, TelefonoIN, "Utilizzatore", "Attivo", ProfessioneIN, CURDATE(), IndirizzoIN, CittaIN, CAPIN);
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE PRENOTA_POSTO_LETTURA(CodiceUtilizzatoreIN INT, NumeroPostoLetturaIN INT, NomeBibliotecaIN VARCHAR(50), DataIN DATE, OraInizioIN TIME, OraFineIN TIME)
BEGIN
	INSERT INTO PrenotazionePostoLettura (CodiceUtilizzatore, NumeroPostoLettura, NomeBiblioteca, Data, OraInizio, OraFine) VALUES
   (CodiceUtilizzatoreIN, NumeroPostoLetturaIN, NomeBibliotecaIN, DataIN, OraInizioIN, OraFineIN);
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE RIMUOVI_PRENOTAZIONE_LETTURA(CodicePrenotazioneIN INT, CodiceUtilizzatoreIN INT)
BEGIN
	DELETE FROM PrenotazionePostoLettura  WHERE Codice = CodicePrenotazioneIN AND CodiceUtilizzatore = CodiceUtilizzatoreIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE PRENOTA_LIBRO(CodiceLibroIN INT, CodiceUtilizzatoreIN INT, DataInizio DATE)
BEGIN
	INSERT INTO Libro (CodiceLibro, CodiceUtilizzatore, DataInizio, DataFine) VALUES 
    (CodiceLibroIN, CodiceUtilizzatoreIN, DataInizioIN, (DataInizioIN + INTERVAL 15 DAY));
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_PRENOTAZIONI(CodiceUtilizzatoreIN INT)
BEGIN
	SELECT P.DataInizio, P.DataFine, P.Codice, L.Titolo, COUNT(DISTINCT(C.Tipo)) AS NEventiConsegna FROM Prenotazione P LEFT JOIN Consegna C ON C.CodicePrenotazione = P.Codice, Libro L WHERE P.CodiceUtilizzatore = CodiceUtilizzatoreIN AND P.CodiceLibro = L.Codice GROUP BY P.Codice;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_PRENOTAZIONI_VOLONTARIO()
BEGIN
SELECT P.Codice, P.DataInizio, P.DataFine, L.StatoPrestito, L.Titolo, L.NomeBiblioteca, U.Nome AS NomeU, U.Cognome AS CognomeU, U.Codice AS CodiceU, U.Indirizzo, U.Citta
FROM Utente U, Prenotazione P, Libro L
WHERE U.Codice = P.CodiceUtilizzatore AND P.CodiceLibro = L.Codice AND 2 > (SELECT COUNT(*) FROM Consegna C WHERE C.CodicePrenotazione = P.Codice);
END $
DELIMITER ;



DELIMITER $
CREATE PROCEDURE VISUALIZZA_PRENOTAZIONE(CodiceIN INT)
BEGIN
	SELECT U.Nome, U.Codice AS CodiceUtilizzatore, U.Cognome, U.Indirizzo, U.Citta, P.DataInizio, P.DataFine, L.Titolo, P.Codice, L.NomeBiblioteca, L.Codice AS CodiceLibro FROM Prenotazione P, Libro L, Utente U WHERE P.Codice = CodiceIN AND P.CodiceUtilizzatore = U.Codice AND P.CodiceLibro = L.Codice;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_PRENOTAZIONI_LETTURA(CodiceUtilizzatoreIN INT)
BEGIN
	SELECT L.Numero, L.NomeBiblioteca, P.Data, P.OraInizio, P.OraFine, L.PresaRete, L.PresaCorrente, P.Codice AS CodicePrenotazione FROM PrenotazionePostoLettura P, PostoLettura L WHERE P.CodiceUtilizzatore = CodiceUtilizzatoreIN  AND P.NumeroPostoLettura = L.Numero AND P.NomeBiblioteca = L.NomeBiblioteca ORDER BY P.Data DESC;

END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_PRENOTAZIONI_LETTURA_BIBLIOTECA(NomeBibliotecaIN VARCHAR(50))
BEGIN
	SELECT U.Nome, U.Cognome, L.Numero, L.NomeBiblioteca, P.Data, P.OraInizio, P.OraFine, L.PresaRete, L.PresaCorrente, P.Codice AS CodicePrenotazione FROM PrenotazionePostoLettura P, PostoLettura L, Utente U WHERE P.NumeroPostoLettura = L.Numero AND U.Codice = P.CodiceUtilizzatore AND P.NomeBiblioteca = NomeBibliotecaIN AND P.NomeBiblioteca = L.NomeBiblioteca ORDER BY P.Data DESC;

END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_CONSEGNE_DA_UTILIZZATORE(CodiceUtilizzatoreIN INT)
BEGIN
	SELECT C.Tipo, C.CodicePrenotazione, C.Note FROM Consegna C, Prenotazione P WHERE P.Codice = C.CodicePrenotazione AND P.CodiceUtilizzatore = CodiceUtilizzatoreIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_CONSEGNE_DA_VOLONTARIO(IdVolontarioIN INT)
BEGIN
	SELECT C.Tipo, L.Titolo, L.NomeBiblioteca, U.Nome, U.Cognome, U.Indirizzo, U.Citta, C.DataConsegna FROM Consegna C, Prenotazione P, Utente U, Libro L WHERE P.Codice = C.CodicePrenotazione AND C.IdVolontario = IdVolontarioIN AND P.CodiceUtilizzatore = U.Codice AND P.CodiceLibro = L.Codice;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_CONSEGNE_DA_PRENOTAZIONE(CodicePrenotazioneIN INT)
BEGIN
	SELECT C.Tipo, C.DataConsegna, C.Note, V.Nome, V.Cognome, V.MezzoTrasporto  FROM Consegna C, Utente V WHERE CodicePrenotazione = CodicePrenotazioneIN AND C.IdVolontario = V.Codice;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE VISUALIZZA_LIBRO(CodiceIN INT)
BEGIN
	SELECT * FROM Libro WHERE Codice = CodiceIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_LIBRO_CARTACEO(CodiceIN INT)
BEGIN
	CALL VISUALIZZA_LIBRO(CodiceIN);
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_EBOOK(CodiceIN INT, CodiceUtilizzatoreIN INT)
BEGIN
	IF NOT EXISTS (SELECT * FROM AccessoEbook WHERE CodiceEbook = CodiceIN AND CodiceUtilizzatore = CodiceUtilizzatoreIN) THEN
		INSERT INTO AccessoEbook VALUES (CodiceIN, CodiceUtilizzatoreIN);
		UPDATE Libro SET NumeroAccessi = NumeroAccessi + 1 WHERE Codice = CodiceIN;
	END IF;
	CALL VISUALIZZA_LIBRO(CodiceIN);
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE INSERISCI_PRENOTAZIONE(CodiceLibroIN INT, CodiceUtilizzatoreIN INT)
BEGIN
INSERT INTO Prenotazione (CodiceLibro, CodiceUtilizzatore) VALUES (CodiceLibroIN, CodiceUtilizzatoreIN);
UPDATE Libro SET StatoPrestito = "Prenotato" WHERE Codice = CodiceLibroIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_AUTORI(CodiceLibroIN INT)
BEGIN
	SELECT A.Nome, A.Cognome, A.Codice FROM Autore A, AutoreLibro L WHERE L.CodiceLibro = CodiceLibroIN AND A.Codice = L.CodiceAutore;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE RIMUOVI_AUTORE(CodiceLibroIN INT, CodiceAutoreIN INT) 
BEGIN
	DELETE FROM AutoreLibro WHERE CodiceLibro = CodiceLibroIN AND CodiceAutore = CodiceAutoreIN;
END $
DELIMITER ;

#### OPERAZIONI SOLO VOLONTARI
DELIMITER $
CREATE PROCEDURE VISUALIZZA_TUTTE_PRENOTAZIONI()
BEGIN
	SELECT U.Nome, U.Cognome, U.Indirizzo, U.Citta, U.CAP, P.Codice AS CodicePrenotazione, P.CodiceLibro AS CodiceLibro, L.Titolo, L.NomeBiblioteca
	FROM Prenotazione P, Utente U, Libro L WHERE P.CodiceLibro = L.Codice AND P.CodiceUtilizzatore = U.Codice;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE INSERISCI_CONSEGNA(TipoIN VARCHAR(30), CodicePrenotazioneIN INT, NoteIN VARCHAR(200), CodiceVolontarioIN INT, DataIN DATE ) 
BEGIN
	IF (TipoIN = "Restituzione" OR TipoIN="Affidamento") THEN
		INSERT INTO Consegna VALUES (TipoIN, CodicePrenotazioneIN, NoteIN, CodiceVolontarioIN, DataIN);
	END IF;
	IF (TipoIN = "Affidamento") THEN
		UPDATE Libro SET StatoPrestito = "Consegnato" WHERE Codice = (SELECT CodiceLibro FROM Prenotazione WHERE Codice = CodicePrenotazioneIN);
		UPDATE Prenotazione SET DataInizio = DataIN, DataFine = DATE_ADD(DataInizio, INTERVAL 15 DAY);
	END IF;
	IF (TipoIN = "Restituzione") THEN
		UPDATE Libro SET StatoPrestito = "Disponibile" WHERE Codice = (SELECT CodiceLibro FROM Prenotazione WHERE Codice = CodicePrenotazioneIN);
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE AGGIORNA_CONSEGNA(TipoIN VARCHAR(30), CodicePrenotazioneIN INT, NoteIN VARCHAR(200))
BEGIN
	UPDATE Consegna SET Note = NoteIN WHERE Tipo = TipoIN AND CodicePrenotazione = CodicePrenotazioneIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_Consegna(CodiceIN INT)
BEGIN
	SELECT * FROM Consegna WHERE Codice = CodiceIN;
END $
DELIMITER ;

#### OPERAZIONI SOLO AMMINISTRATORE


DELIMITER $
CREATE PROCEDURE INSERIMENTO_LIBRO_CARTACEO(NomeBibliotecaIN VARCHAR(50), TitoloIN VARCHAR(30), AnnoPubblicazioneIN  CHAR(4), NomeEdizioneIN VARCHAR(20),  StatoConservazioneIN VARCHAR(30))
BEGIN
	INSERT INTO Libro (NomeBiblioteca, Titolo, AnnoPubblicazione,NomeEdizione,Tipo, StatoPrestito,StatoConservazione) VALUES (NomeBibliotecaIN, TitoloIN, AnnoPubblicazioneIN, NomeEdizioneIN, "Cartaceo", "Disponibile", StatoConservazioneIN); 
	SELECT LAST_INSERT_ID() AS ID;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE `INSERIMENTO_LIBRO_EBOOK`(NomeBibliotecaIN VARCHAR(50), TitoloIN VARCHAR(30), AnnoPubblicazioneIN INT, NomeEdizioneIN VARCHAR(30), PDFIN VARCHAR(50), DimensioneIN DOUBLE, GenereIN VARCHAR(30))
BEGIN
	INSERT INTO Libro (NomeBiblioteca, Titolo, AnnoPubblicazione, NomeEdizione, Tipo, PDF, Dimensione, NumeroAccessi, Genere) VALUES
    (NomeBibliotecaIN, TitoloIN, AnnoPubblicazioneIN, NomeEdizioneIN, "Ebook", PDFIN, DimensioneIN, 0, GenereIN);
	SELECT LAST_INSERT_ID() AS ID;
END $
DELIMITER ;



DELIMITER $
CREATE PROCEDURE `INSERIMENTO_AUTORE`(NomeIN VARCHAR(40), CognomeIN VARCHAR(40))
BEGIN
	INSERT INTO Autore (Nome, Cognome) VALUES (NomeIN, CognomeIN);
	SELECT LAST_INSERT_ID() AS ID;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE `INSERIMENTO_AUTORE_LIBRO`(CodiceLibroIN INT, CodiceAutoreIN INT)
BEGIN
	INSERT INTO AutoreLibro (CodiceLibro, CodiceAutore) VALUES (CodiceLibroIN, CodiceAutoreIN);
	SELECT LAST_INSERT_ID() AS ID;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE AGGIUNGI_PDF_EBOOK(CodiceLibroIN INT, PathIN VARCHAR(50), DimensioneIN DOUBLE)
BEGIN
	UPDATE Libro SET PDF = PathIN, Dimensione = DimensioneIN WHERE Codice = CodiceLibroIN; 
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE ELIMINA_LIBRO(CodiceLibroIN INT)
BEGIN 
	DELETE FROM Libro WHERE Codice = CodiceLibroIN;
	DELETE FROM AutoreLibro WHERE CodiceLibro = CodiceLibroIN;
	DELETE FROM AccessoEbook WHERE CodiceEbook = CodiceLibroIN;
	DELETE FROM Prenotazione WHERE CodiceLibro = CodiceLibroIN;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE MODIFICA_LIBRO_CARTACEO(CodiceLibroIN INT, NomeBibliotecaIN VARCHAR(50), TitoloIN VARCHAR(30), AnnoPubblicazioneIN INT, NomeEdizioneIN VARCHAR(30), StatoConservazioneIN VARCHAR(15), StatoPrestitoIN VARCHAR(30), GenereIN VARCHAR(30), NumeroPagineIN INT, NumeroScaffaleIN INT)
BEGIN
    IF (StatoConservazioneIN = "Ottimo" OR StatoConservazioneIN = "Buono" OR StatoConservazioneIN = "Non Buono" OR StatoConservazioneIN = "Scadente") AND (StatoPrestitoIN = "Disponibile" OR StatoPrestitoIN = "Prenotato" OR StatoPrestitoIN = "Consegnato") THEN
		UPDATE Libro SET NomeBiblioteca = NomeBibliotecaIN, Titolo = TitoloIN,AnnoPubblicazione = AnnoPubblicazioneIN, NomeEdizione = NomeEdizioneIN, StatoPrestito = StatoPrestitoIN, StatoConservazione = StatoConservazioneIN, Genere = GenereIN, NumeroPagine = NumeroPagineIN, NumeroScaffale = NumeroScaffaleIN
		WHERE Codice = CodiceLibroIN;
    ELSE
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'StatoConservazione o StatoPrestito non valido'; 
    END IF;
END $ 
DELIMITER ;


DELIMITER $
CREATE PROCEDURE INSERISCI_MESSAGGIO(CodiceUtilizzatoreIN INT, CodiceAmministratoreIN INT, TitoloIN VARCHAR(50), TestoIN VARCHAR(300))
BEGIN
	INSERT INTO Messaggio (CodiceUtilizzatore, CodiceAmministratore, Titolo, Testo, Dataa) VALUES 
    (CodiceUtilizzatoreIN, CodiceAmministratoreIN, TitoloIN, TestoIN, CURDATE());
END $ 
DELIMITER ;

DELIMITER $
CREATE PROCEDURE INSERISCI_SEGNALAZIONE(CodiceUtilizzatoreIN INT, CodiceAmministratoreIN INT, TestoIN VARCHAR(300))
BEGIN
	INSERT INTO Segnalazione (CodiceUtilizzatore, CodiceAmministratore, Testo, Dataa) VALUES 
    (CodiceUtilizzatoreIN, CodiceAmministratoreIN, TestoIN, CURDATE());
	IF((SELECT COUNT(*) FROM Segnalazione WHERE Codice = CodiceUtilizzatoreIN) > 3) THEN 
   		UPDATE Utente 
   		SET StatoAccount = "Sospeso"
   		WHERE Codice = CodiceUtilizzatoreIN;
   	
   	END IF;
END $ 
DELIMITER ;

DELIMITER $
CREATE PROCEDURE RIMUOVI_SEGNALAZIONI(CodiceUtilizzatoreIN INT)
BEGIN
	DELETE FROM Segnalazione WHERE CodiceUtilizzatore = CodiceUtilizzatoreIN;
    UPDATE Utente SET StatoAccount = "Attivo" WHERE Codice = CodiceUtilizzatoreIN;
END $ 
DELIMITER ;

CREATE VIEW NUMERO_PRENOTAZIONI_UTENTE AS
SELECT U.Codice AS CodiceUtilizzatore, COUNT(P.Codice) AS NPrenotazioni FROM Utente U LEFT JOIN  Prenotazione P ON P.CodiceUtilizzatore = U.Codice GROUP BY U.Codice;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_DATI_CLUSTERING()
BEGIN
	SELECT * FROM Utente U, NUMERO_PRENOTAZIONI_UTENTE N WHERE TipoUtente = "Utilizzatore" AND U.Codice = N.CodiceUtilizzatore;
END $ 
DELIMITER ;


### STATISTICHE


CREATE VIEW NUMERO_POSTI_LETTURA_LIBERI AS
SELECT B.Nome, COUNT(L.Numero) as NumeroPostiLiberi FROM Biblioteca B, PostoLettura L
WHERE B.Nome = L.NomeBiblioteca AND L.Numero NOT IN (SELECT P.NumeroPostoLettura FROM PrenotazionePostoLettura P WHERE P.NomeBiblioteca = L.NomeBiblioteca
AND P.Data = CURDATE() AND P.OraInizio < DATE_FORMAT(CONVERT_TZ(NOW(), '+00:00','+01:00'), '%H:%i:%s') AND P.OraFine > DATE_FORMAT(CONVERT_TZ(NOW(), '+00:00','+01:00'), '%H:%i:%s'))
GROUP BY B.Nome;



DELIMITER $
CREATE PROCEDURE STATISTICHE_BIBLIOTECHE_LIBERE()
BEGIN
SELECT B.Nome, (N.NumeroPostiLiberi / P.PostiLettura) AS PercentualePosti FROM Biblioteca B, NUMERO_POSTI_LETTURA_LIBERI N, NUMERO_POSTI_LETTURA P
WHERE B.Nome = N.Nome AND B.Nome = P.Biblioteca
GROUP BY B.Nome
ORDER BY (N.NumeroPostiLiberi / P.PostiLettura) DESC
LIMIT 5;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE STATISTICHE_EBOOK()
BEGIN
SELECT L.Titolo, L.NumeroAccessi FROM Libro L WHERE Tipo = "Ebook" ORDER BY L.NumeroAccessi DESC LIMIT 5;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE STATISTICHE_CARTACEI()
BEGIN
SELECT L.Titolo, COUNT(P.Codice) AS NumeroPrenotazioni FROM Libro L, Prenotazione P WHERE L.Codice = P.CodiceLibro GROUP BY L.Codice
ORDER BY COUNT(P.Codice) DESC LIMIT 5;
END $
DELIMITER ;


CREATE VIEW NUMERO_CONSEGNE_VOLONTARIO AS
SELECT U.Codice, U.Nome, U.Cognome, COUNT(*) AS NumeroConsegne FROM Utente U, Consegna C WHERE U.Codice = C.idVolontario GROUP BY U.Codice;

DELIMITER $
CREATE PROCEDURE STATISTICHE_VOLONTARI()
BEGIN
SELECT Nome, Cognome, NumeroConsegne FROM NUMERO_CONSEGNE_VOLONTARIO ORDER BY NumeroConsegne DESC LIMIT 5;
END $
DELIMITER ;


DELIMITER $
	CREATE PROCEDURE FILTRA_LIBRI(CodLibroIN INT, TitoloIN VARCHAR(100), AutoreIN VARCHAR(300), AnnoPubblicazioneIN INT, TipoLibroIN ENUM("Cartaceo", "Ebook"), BibliotecaIN VARCHAR(50))
	BEGIN
	SELECT L.* FROM Libro L 
	WHERE (BibliotecaIN LIKE "" OR BibliotecaIN LIKE L.NomeBiblioteca) AND (TitoloIN LIKE "" OR L.Titolo LIKE CONCAT('%', TitoloIN, '%')) AND (CodLibroIN = 0 OR CodLibroIN = L.Codice)
	AND (AnnoPubblicazioneIN = 0 OR AnnoPubblicazioneIN = L.AnnoPubblicazione) AND (TipoLibroIN LIKE "" OR TipoLibroIN = L.Tipo) 
    AND (AutoreIN LIKE "" OR EXISTS (SELECT * FROM AutoreLibro L, Autore A WHERE L.CodiceAutore = A.Codice AND L.CodiceLibro = L.Codice AND A.Cognome LIKE CONCAT('%', AutoreIN, '%'))  );
	END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE VISUALIZZA_PAGINA_RIEPILOGO(CodicePrenotazioneIN int )
BEGIN
	SELECT 	L.Codice as idLibro ,L.Titolo , L.Genere , L.AnnoPubblicazione , L.NomeEdizione , L.Tipo as TipoLibro , 
		A.Nome as NomeAutore , A.Cognome as CognomeAutore , 
		U.Codice as CodiceUtente , U.Nome as NomeUtente , U.Cognome as CognomeUtente, U.MezzoTrasporto, 
		C.Note, C.Tipo as TipoConsegna
	FROM Libro as L , Prenotazione as P , Utente as U , AutoreLibro as AL , Autore as A, Consegna as C
	WHERE L.Codice = P.CodiceLibro AND P.CodiceUtilizzatore = U.Codice and 
		  L.Codice = AL.CodiceLibro AND A.Codice = AL.CodiceAutore AND 
		  C.CodicePrenotazione = CodicePrenotazioneIN AND 
		  (U.TipoUtente like "Volontario")
		  ;
	
END $
DELIMITER ; 
