
##Amministratori

INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES(1, 'Mario', 'Rossi', 'mario.rossi@unibo.it', '$2y$10$VDQg/gQc9I6.cVZxyjgToOCXjq5vkCnvZmghtGtVOxe8SlIf9zlbi', '1968-09-11', 'Bologna', '3476719398', 'Amministratore', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Impiegato', 'Walter Bigiavi');

INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES(2, 'Luigi', 'Verdi', 'luigi.verdi@unibo.it', '$2y$10$VDQg/gQc9I6.cVZxyjgToOCXjq5vkCnvZmghtGtVOxe8SlIf9zlbi', '1970-10-23', 'Modena', '3476739858', 'Amministratore', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Impiegato', 'Biblioteca Universitaria');


##Volontari

INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES (3, 'Andrea', 'Accornero', 'andrea@unibo.it', '$2y$10$WQCTsuKQnqHwVkUwSg3bROZX.iAT5KrJGHAXUXVqyTYiqog8sOcL2', '1999-05-14', 'Savona', '3488180258', 'Volontario', 'Bici', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES (4, 'Giacomo', 'Orsi', 'giacomo@unibo.it', '$2y$10$WQCTsuKQnqHwVkUwSg3bROZX.iAT5KrJGHAXUXVqyTYiqog8sOcL2', '1999-09-27', 'Savona', '3455678912', 'Volontario', 'Monopattino', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


##Utenti

INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES (5, 'Nicolo', 'Sguerso', 'nicolo@gmail.com', '$2y$10$zhtIu8l1Ti6cOfODyxG79ObTfYZX2PgVNd2BF0XyTpmnu.ANWQR8e', '1999-03-10', 'Genova', '3476719398', 'Utilizzatore', NULL, '2021-04-28', 'Attivo', 'Studente', 'Via Donato Creti, 18', 'Bologna', '40128', NULL, NULL);


INSERT INTO `Utente` (`Codice`, `Nome`, `Cognome`, `Email`, `Password`, `DataNascita`, `LuogoNascita`, `Telefono`, `TipoUtente`, `MezzoTrasporto`, `DataCreazione`, `StatoAccount`, `Professione`, `Indirizzo`, `Citta`, `CAP`, `Qualifica`, `Biblioteca`)
VALUES (6, 'Elon', 'Musk', 'elon@gmail.com', '$2y$10$zhtIu8l1Ti6cOfODyxG79ObTfYZX2PgVNd2BF0XyTpmnu.ANWQR8e', '1999-06-28', 'Pasadina', '3476719398', 'Utilizzatore', NULL, '2021-04-25', 'Attivo', 'Studente', 'Piazza Maggiore, 3', 'Bologna', '40128', NULL, NULL);


##Biblioteche

INSERT INTO `Biblioteca` (`Nome`, `Latitudine`, `Longitudine`, `NoteStoriche`, `Indirizzo`, `SitoWeb`)
VALUES
	('Biblioteca Centrale del Campus di Rimini', 44.063619, 12.568992, X'412052696D696E692074726F76657265746520756E2043616D70757320646F76652073766F6C676F6E6F206C61206C6F726F2061747469766974612720646F63656E746920652072696365726361746F72692067696F76616E692065206D6F7469766174692C2061737369656D65206120706572736F6E616C65207465636E69636F2D616D6D696E69737472617469766F20636F6D706574656E746520652073656D70726520617474656E746F20616C6C6520766F737472652065736967656E7A652E0A0A4C276F66666572746120666F726D617469766120652720726963636120646920706572636F72736920646920656C6576617461207175616C6974612720652062656E20636F6C6C656761746920616C6C65207370656369666963697461272064656C207465727269746F72696F2E20496E6F6C747265207661206372657363656E646F20646920616E6E6F20696E20616E6E6F20696C206E756D65726F2064656920636F7273692064692073747564696F20696D7061727469746920696E746572616D656E746520696E206C696E67756120696E676C6573652C206120756C746572696F72652074657374696D6F6E69616E7A612064656C6C6120666F72746520766F63617A696F6E6520696E7465726E617A696F6E616C652064656C2043616D70757320652064656C6C612063697474612720636865206C6F206F73706974612E20', 'Via Vittime Civili di guerra, 5 Rimini', 'https://www.unibo.it/it/campus-rimini/biblioteca'),
	('Biblioteca di Chimica', 44.496163, 11.354146, X'44616C2031393837206E756F76612064656E6F6D696E617A696F6E652064656C6C61206269626C696F746563612064656C6C27497374697475746F206368696D69636F2022472E204369616D696369616E222E2044616C2030312E30312E32303135206E756F76612064656E6F6D696E617A696F6E652064656C6C61204269626C696F746563612064656C20446970617274696D656E746F206469206368696D6963612E', 'Via Selmi 2, Bologna', 'http://www.chimica.unibo.it/it/biblioteca'),
	('Biblioteca di Matematica', 44.498138, 11.355920, X'4C61204269626C696F74656361206469204D6174656D61746963612C204669736963612C20417374726F6E6F6D6961206520496E666F726D6174696361202D2042494D4641492065272070726F6D6F7373612064616920446970617274696D656E7469206469204D6174656D61746963612C20646920466973696361206520417374726F6E6F6D69612C206520646920496E666F726D6174696361202D20536369656E7A61206520496E6765676E657269612E0A', 'P.zza Porta San Donato 5, Bologna', 'http://bimfai.sba.unibo.it'),
	('Biblioteca Universitaria', 44.497083, 11.353006, X'4C61204269626C696F7465636120556E697665727369746172696120646920426F6C6F676E61202D2042554220E8206C61206269626C696F746563612063656E7472616C652064656C6C3F4174656E656F20656420E820617065727461206120747574746920636F6C6F726F206368652064657369646572616E6F20636F6E73756C746172652C206C6567676572652C2073747564696172652C20726963657263617265206F2073656D706C6963656D656E746520637572696F736172652074726120676C69207363616666616C692064656C6C652073756520636F6C6C657A696F6E692E0A0A53697461206E656C2063756F72652070756C73616E74652064656C6C6120636974746164656C6C6120756E69766572736974617269612C20616C6C27696E7465726E6F2064656C6C27616E7469636F2050616C617A7A6F20506F6767692C206C61204269626C696F7465636120556E697665727369746172696120E820756E206C756F676F20646564696361746F20616C6C61206C6574747572612C20616C6C612072696365726361206520616C6C6F2073747564696F20652068612C2074726120692073756F69206F62696574746976692C206C612070726F6D6F7A696F6E652064656C207269636368697373696D6F2070617472696D6F6E696F20646F63756D656E74616C652C20666F746F6772616669636F2C2073746F7269636F20652061727469737469636F2E204261737461206F6C7472657061737361726520696C2073756F20706F72746F6E65207065722074726F766172736920696E20756E206C756F676F206163636F676C69656E74652065206F73706974616C652C20636972636F6E64617469206461206C696272692C2071756164726920652061727265646920646920726172612062656C6C657A7A612E', 'Via Zamboni 33, Bologna', 'https://bub.unibo.it'),
	('Walter Bigiavi', 44.497623, 11.351852, X'4C61204269626C696F74656361206469204469736369706C696E652065636F6E6F6D69636F2D617A69656E64616C69202757616C746572204269676961766927206E617363652064616C6C27756E696F6E652064656C6C61204269626C696F74656361206469204469736369706C696E652065636F6E6F6D69636865202757616C74657220426967696176692720636F6E206C65206269626C696F74656368652064656920646970617274696D656E746920646920536369656E7A6520617A69656E64616C69206520646920536369656E7A652065636F6E6F6D696368652C206F666672656E646F20636F73692720756E6F207370617A696F20636F6D756E6520646920646F63756D656E74617A696F6E65206520726963657263612070657220676C692073747564656E74692C20692072696365726361746F72692065206920646F63656E74692064656C20736574746F72652E0A4C612073656465206527207175656C6C612064656C6C61204269626C696F74656361202757616C7465722042696769617669272C20696E207669612064656C6C652042656C6C6520417274692033332C206D6120636920736F6E6F20616E636865206465706F7369746920652073616C65206469206C657474757261206E65676C69207370617A69206164696163656E7469206469207069617A7A6120536361726176696C6C6920312E0A4C6520747265206269626C696F7465636865206E656C206C75676C696F2064656C20323032302068616E6E6F206D6573736F20696E20636F6D756E6520696C206C6F726F2070617472696D6F6E696F206469206369726361203234302E30303020766F6C756D692065206F6C74726520342E30303020706572696F646963692E', 'Via delle Belle Arti 33, Bologna', 'http://bigiavi.sba.unibo.it/');


INSERT INTO `Immagine` (`Nome`, `NomeBiblioteca`)
VALUES
	('bigiavi-1.jpeg', 'Walter Bigiavi'),
	('bigiavi-2.jpg', 'Walter Bigiavi'),
	('bigiavi-3.jpg', 'Walter Bigiavi'),
	('chimica-1.jpeg', 'Biblioteca di Chimica'),
	('matematica-1.jpeg', 'Biblioteca di Matematica'),
	('matematica-2.jpeg', 'Biblioteca di Matematica'),
	('rimini.jpeg', 'Biblioteca Centrale del Campus di Rimini');


##PostoLettura
INSERT INTO `PostoLettura` (`Numero`, `NomeBiblioteca`, `PresaRete`, `PresaCorrente`)
VALUES
	(1, 'Biblioteca Universitaria', 1, 0),
	(1, 'Walter Bigiavi', 1, 0),
	(2, 'Biblioteca Universitaria', 0, 1),
	(2, 'Walter Bigiavi', 0, 1),
	(3, 'Biblioteca Universitaria', 1, 0),
	(3, 'Walter Bigiavi', 1, 0),
	(4, 'Biblioteca Universitaria', 0, 0),
	(4, 'Walter Bigiavi', 0, 0),
	(5, 'Biblioteca Universitaria', 1, 1),
	(5, 'Walter Bigiavi', 1, 1),
	(6, 'Biblioteca Universitaria', 1, 0),
	(6, 'Walter Bigiavi', 1, 0),
	(7, 'Biblioteca Universitaria', 0, 0),
	(7, 'Walter Bigiavi', 0, 0),
	(8, 'Biblioteca Universitaria', 0, 1),
	(8, 'Walter Bigiavi', 0, 1),
	(9, 'Biblioteca Universitaria', 1, 1),
	(9, 'Walter Bigiavi', 1, 1),
	(10, 'Biblioteca Universitaria', 1, 0),
	(10, 'Walter Bigiavi', 1, 0),
	(11, 'Biblioteca Universitaria', 0, 0),
	(11, 'Walter Bigiavi', 0, 0),
	(12, 'Biblioteca Universitaria', 1, 1),
	(13, 'Biblioteca Universitaria', 0, 0);


## Recapiti
INSERT INTO `Recapito` (`Numero`, `NomeBiblioteca`)
VALUES
	('0512099457', 'Biblioteca di Chimica'),
	('051457865', 'Walter Bigiavi'),
	('051473215', 'Biblioteca Universitaria');



##Autori
INSERT INTO `Autore` (`Codice`, `Nome`, `Cognome`)
VALUES
	(1, 'Valerio', 'Lundini'),
	(2, 'Bernie', 'Senders'),
	(3, 'Keith', 'Richards'),
	(4, 'Jeff', 'Pearlman'),
	(5, 'Michelle', 'Obama'),
	(6, 'George', 'Orwell'),
	(7, 'William', 'Golding'),
	(8, 'George', 'Orwell'),
	(9, 'David', 'Parenzo'),
	(10, 'Carlo', 'Rovelli');


##Libri
INSERT INTO `Libro` (`Codice`, `NomeBiblioteca`, `Titolo`, `AnnoPubblicazione`, `NomeEdizione`, `Tipo`, `StatoPrestito`, `StatoConservazione`, `PDF`, `Dimensione`, `NumeroAccessi`, `Genere`, `NumeroPagine`, `NumeroScaffale`)
VALUES
	(1, 'Walter Bigiavi', 'Era meglio il libro', 2020, 'Prima', 'Cartaceo', 'Consegnato', 'Ottimo', NULL, NULL, NULL, 'Satira', 234, 12),
	(2, 'Walter Bigiavi', 'La più grande sfida', 2016, 'Quarta', 'Cartaceo', 'Disponibile', 'Buono', NULL, NULL, NULL, 'Politica', 534, 2),
	(3, 'Biblioteca Universitaria', 'Life', 2010, 'Speciale', 'Cartaceo', 'Disponibile', 'Non Buono', NULL, NULL, NULL, 'Biografico', 800, 1),
	(4, 'Biblioteca Universitaria', 'Three Ring Circus', 2021, 'Inglese', 'Cartaceo', 'Consegnato', 'Ottimo', NULL, NULL, NULL, 'Sportivo', 460, 10),
	(28, 'Walter Bigiavi', 'I Falsari', 2019, 'Seconda', 'Ebook', NULL, NULL, '/ebooks/28.pdf', 8.203, 1, 'Politica', NULL, NULL),
	(29, 'Biblioteca Universitaria', '1984', 1950, 'Cult', 'Ebook', NULL, NULL, '/ebooks/29.pdf', 8.203, 1, 'Fantascienza', NULL, NULL);



##AutoreLibro

INSERT INTO `AutoreLibro` (`CodiceLibro`, `CodiceAutore`)
VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(28, 9),
	(29, 6);


##Segnalazioni
INSERT INTO `Segnalazione` (`Codice`, `CodiceAmministratore`, `CodiceUtilizzatore`, `Dataa`, `Testo`)
VALUES
	(1, 1, 5, '2021-04-20', 'Prima segnalazione prova'),
	(2, 2, 5, '2021-04-20', 'Seconda segnalazione prova'),
	(3, 1, 6, '2021-04-20', 'Terza segnalazione prova');


##Messaggi
INSERT INTO `Messaggio` (`Codice`, `CodiceAmministratore`, `CodiceUtilizzatore`, `Dataa`, `Testo`, `Titolo`)
VALUES
	(1, 1, 6, '2021-04-23', 'Primo messaggio prova', 'Titolo Prova'),
	(2, 2, 6, '2021-04-23', 'Secondo messaggio prova', 'Titolo Prova'),
	(3, 2, 5, '2021-04-23', 'Terzo messaggio prova', 'Titolo Prova');

#Prenotazioni
INSERT INTO `Prenotazione` (`Codice`, `CodiceLibro`, `CodiceUtilizzatore`, `DataInizio`, `DataFine`)
VALUES
	(14, 4, 5, '2021-04-30', '2021-05-15'),
	(15, 1, 6, '2021-04-30', '2021-05-15');

#Consegne
INSERT INTO `Consegna` (`Tipo`, `CodicePrenotazione`, `Note`, `IdVolontario`, `DataConsegna`)
VALUES
	('Affidamento', 14, '', 3, '2021-04-30'),
	('Affidamento', 15, '', 3, '2021-04-30');






