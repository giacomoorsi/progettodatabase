CREATE TABLE Utente (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    Nome VARCHAR(30), 
    Cognome VARCHAR(30),
    Email VARCHAR(30),
    Password VARCHAR(60),
    DataNascita DATE,
    LuogoNascita VARCHAR(30),
    Telefono VARCHAR(15),
    TipoUtente ENUM("Volontario", "Utilizzatore", "Amministratore"),
    MezzoTrasporto VARCHAR(30), # Volontario
    DataCreazione DATE,  # Utilizzatore 
    StatoAccount ENUM("Attivo", "Sospeso"), # Utilizzatore
    Professione VARCHAR(30), # Utilizzatore
    Indirizzo VARCHAR(50), # Utilizzatore
    Citta VARCHAR(30), # Utilizzatore
    CAP CHAR(5),
    Qualifica VARCHAR(30) # Amministratore,
    Biblioteca VARCHAR(50) REFERENCES Biblioteca(Nome)
);

CREATE TABLE Biblioteca (
    Nome VARCHAR(50) PRIMARY KEY,
    Latitudine Decimal(8,6),
    Longitudine Decimal(9,6),
    NoteStoriche BLOB, 
    Indirizzo VARCHAR(50),
    SitoWeb VARCHAR(50)
); 

CREATE TABLE Immagine (
    Nome VARCHAR(50), 
    NomeBiblioteca VARCHAR(50) REFERENCES Biblioteca (Nome),
    PRIMARY KEY (Nome, NomeBiblioteca)
);

CREATE TABLE Recapito (
    Numero VARCHAR(15) PRIMARY KEY,
    NomeBiblioteca VARCHAR(50) NOT NULL REFERENCES Biblioteca (Nome) 
);

CREATE TABLE PostoLettura (
    Numero INT, 
    NomeBiblioteca VARCHAR(50) REFERENCES Biblioteca(Nome),
    PresaRete BOOLEAN,
    PresaCorrente BOOLEAN,
    PRIMARY KEY(Numero, NomeBiblioteca)
);

CREATE TABLE Autore (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    Nome VARCHAR(30), 
    Cognome VARCHAR(30)
);

CREATE TABLE Libro (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    NomeBiblioteca VARCHAR(50) REFERENCES Biblioteca (Nome),
    Titolo VARCHAR(30),
    AnnoPubblicazione INT,
    NomeEdizione VARCHAR(30),
    Tipo ENUM("Cartaceo", "Ebook"),
    StatoPrestito ENUM("Disponibile", "Prenotato", "Consegnato"), # LibroCartaceo
    StatoConservazione ENUM("Ottimo", "Buono", "Non Buono", "Scadente"), # Libro Cartaceo
    PDF VARCHAR(30), # Ebook - indica la posizione del file
    Dimensione DOUBLE, # Ebook - Dimensione in Megabyte
    NumeroAccessi INT, # Ebook
    Genere VARCHAR(30),
    NumeroPagine INT,
    NumeroScaffale INT
);

CREATE TABLE Prenotazione (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    CodiceLibro INT REFERENCES Libro (Codice),
    CodiceUtilizzatore INT REFERENCES Utente (Codice), 
    DataInizio DATE,
    DataFine DATE # Valore di default ? 
);

CREATE TABLE Consegna (
    Tipo ENUM("Restituzione", "Affidamento"),
    CodicePrenotazione INT REFERENCES Prenotazione(Codice),
    Note VARCHAR(200),
    IdVolontario int REFERENCES Utente(Codice),
    DataConsegna DATE,
    PRIMARY KEY (Tipo, CodicePrenotazione)
);

CREATE TABLE Messaggio (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    CodiceAmministratore INT REFERENCES Utente(Codice),
    CodiceUtilizzatore INT REFERENCES Utente(Codice),
    Dataa DATE,
    Testo VARCHAR(300),
    Titolo VARCHAR(50)
);

CREATE TABLE Segnalazione (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    CodiceAmministratore INT REFERENCES Utente(Codice),
    CodiceUtilizzatore INT REFERENCES Utente(Codice),
    Dataa DATE,
    Testo VARCHAR(300)
);

CREATE TABLE PrenotazionePostoLettura (
    Codice INT AUTO_INCREMENT PRIMARY KEY,
    CodiceUtilizzatore INT REFERENCES Utente(Codice),
    NumeroPostoLettura INT REFERENCES PostoLettura (Numero),
    NomeBiblioteca VARCHAR(50) REFERENCES PostoLettura (Nome),
    Data DATE,
    OraInizio TIME,
    OraFine TIME
);

CREATE TABLE AccessoEbook (
    CodiceEbook INT REFERENCES Libro(Codice),
    CodiceUtilizzatore INT REFERENCES Utente(Codice),
    PRIMARY KEY (CodiceEbook, CodiceUtilizzatore)
);


CREATE TABLE AutoreLibro (
    CodiceLibro INT REFERENCES Libro(Codice),
    CodiceAutore INT REFERENCES Autore(Codice),
    PRIMARY KEY (CodiceLibro, CodiceAutore)
);