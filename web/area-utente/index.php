<?php
    // /area-utente/index.php
    session_start();
    if(!isset($_SESSION["Codice"])){
        header("Location: /login.php");
    } 
    if($_SESSION["StatoAccount"] == "Sospeso"){
        header("Location: elenco-segnalazioni.php");
    } else {
        header("Location: prenotazioni-libri.php");
    }

?>