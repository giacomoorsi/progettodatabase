<?php
include '../utilities/functions.php';


if(!isset($_SESSION["Codice"])){
    header("Location: /login.php");
    die(0);
}

if(!isset($_GET["codice"])){
    header("Location: prenotazioni-lettura.php");
    die(0);
}


$query = $db->prepare('CALL RIMUOVI_PRENOTAZIONE_LETTURA(:codicePrenotazione, :codUtilizzatore);');
$query->execute(array(
    ':codicePrenotazione' => $_GET["codice"],
    ':codUtilizzatore' => $_SESSION["Codice"]
));
inviaLog("Codice Utente :".$_SESSION["Codice"]." - Operazione : Rimozione Prenotazione - id Prenotazione : ". $_GET["codice"]);

header('Location: prenotazioni-lettura.php?eliminata=1');


?>