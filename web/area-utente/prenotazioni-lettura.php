<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Prenotazione posti lettura</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>

    <?php
    if(isset($_GET["eliminata"])){
        echo '<script>UIkit.modal.alert("La prenotazione per il posto lettura è stata cancellata.").then();</script>';
    }

    $query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONI_LETTURA(:codiceUt);");
    $query->execute(array(
        ':codiceUt' => $_SESSION["Codice"]
    ));
    ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Prenotazioni posti lettura</h1>



        <?php 
        $mostratoTitoloDateVecchie = false;
        $presente = false;
        while($prenotazione = $query->fetch(PDO::FETCH_ASSOC)) {  
            $presente = true;
        if(date_create_from_format('Y-m-d', $prenotazione["Data"]) < date_create_from_format('Y-m-d', date('Y-m-d'))){
            $prenotazioneVecchia = true;
            if(!$mostratoTitoloDateVecchie){
                echo '<h3>Prenotazioni passate</h3>';
                $mostratoTitoloDateVecchie = true;
            }
        } else {
            $prenotazioneVecchia = false;
        } ?>

        <div class="uk-card uk-margin uk-card-default uk-card-body">
            <div class="uk-card-badge  uk-label">Prenotazione n. <?=$prenotazione["CodicePrenotazione"]?></div>
            <div uk-grid>
                <div class="uk-width-expand">
                    <h4 class="uk-text-muted uk-margin-remove"><?=$prenotazione["NomeBiblioteca"]?></h4>
                    <h3 class="uk-margin-remove">Posto n. <?=$prenotazione["Numero"]?></h3>
                </div>
                <div class="uk-width-auto uk-text-right">
                </div>
            </div>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div class="<?= $prenotazione["PresaRete"] ? "uk-text-success" : "uk-text-danger"?>"><span uk-icon="world"> </span> Connessione ethernet</div>
                <div class="<?= $prenotazione["PresaCorrente"] ? "uk-text-success" : "uk-text-danger"?>"><span uk-icon="bolt"></span> Presa corrente</div>
                <div><span uk-icon="calendar"></span> <?=date_format(date_create_from_format('Y-m-d', $prenotazione["Data"]), 'd/m/Y')?></div>
                <div><span uk-icon="clock"></span> <?=substr($prenotazione["OraInizio"], 0, 5)?> - <?=substr($prenotazione["OraFine"], 0, 5)?></div>
                <?php if(!$prenotazioneVecchia) { ?> <div>
                    <a href="/area-utente/cancella-prenotazione-lettura.php?codice=<?=urlencode($prenotazione["CodicePrenotazione"])?>" class="uk-button uk-button-text">Cancella</a>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } 
        if(!$presente){
            echo '<p>Non hai nessuna prenotazione per un posto lettura in biblioteca.</p>';
        } ?>
        



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.prenotazioni-lettura>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>

</html>