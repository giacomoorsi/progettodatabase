<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Elenco prenotazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco prenotazioni</h1>

    <?php


    $query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONI(:codiceUtilizzatore)");
    $query->execute(array(
    ':codiceUtilizzatore' => $_SESSION["Codice"]
    ));

    $presenti_prenotazioni = false;

    while($prenotazione = $query->fetch(PDO::FETCH_ASSOC)){
        $presenti_prenotazioni = true;

        if($prenotazione["NEventiConsegna"]==0){
            $stato = "Da consegnare";
        } else if ($prenotazione["NEventiConsegna"]==1){
            $stato = "Consegnato";
        } if ($prenotazione["NEventiConsegna"]==2){
            $stato = "Conclusa";
        }


        echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            
            <h2 class="uk-margin-remove">'.$prenotazione["Titolo"].'</h3>
            <div class="uk-card-badge  uk-label">'.$stato.'</div>
            <div class="uk-grid-small uk-margin-small" uk-grid>';
        if($stato != "Da consegnare"){
            echo '<div><span uk-icon="calendar"> </span>'.date_format(date_create_from_format('Y-m-d',$prenotazione["DataInizio"]), 'd/m/Y')." - ".date_format(date_create_from_format('Y-m-d',$prenotazione["DataFine"]), 'd/m/Y').'</div>';

        } echo '<div><span uk-icon="hashtag"></span> Prenotazione n. '.$prenotazione["Codice"] . '</div>
                <div class="uk-expand uk-text-right"><a href="/area-utente/prenotazione.php?id='.$prenotazione["Codice"].'" class="uk-button uk-button-text">Apri prenotazione</a></div>
            </div>
        </div>';
    
    }


    if(!$presenti_prenotazioni){
        echo '<p>Non hai prenotazione attive o passate.</p>';
    }
    ?>



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.prenotazioni-libri>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
