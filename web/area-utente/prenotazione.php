<?php
require '../utilities/functions.php';
if(!isset($_GET["id"])){
        header("location:elenco-prenotazioni-biblioteca.php");
        die(0);
}

$idPrenotazione = $_GET["id"];
$query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONE(:idPrenotazione);");
$query->execute(array(
    ':idPrenotazione' => $idPrenotazione
));

$prenotazione = $query->fetchAll(PDO::FETCH_ASSOC)[0];
$query->closeCursor();


?>
<!DOCTYPE html>
<html>

<head>
    <title>Riepilogo prenotazione</title>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php';
    include '../template/left-bar.php';
    ?>
    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <div class="uk-card uk-margin uk-card-default uk-card-body">
            <h2 class="uk-margin-remove">Prenotazione n. <?=$prenotazione["Codice"]?></h2>
            <div class="uk-grid uk-margin-small "  uk-grid>
                <div><span uk-icon="file-text"></span> <?=$prenotazione["Titolo"];?></div>
                <div><span uk-icon="user"> </span> <?=$prenotazione["Nome"]?>
                    <?=$prenotazione["Cognome"]?></div>
                <?php if ($prenotazione["DataInizio"] != "") { ?>
                <div><span uk-icon="calendar"></span> dal
                    <?=date_format(date_create_from_format('Y-m-d',$prenotazione["DataInizio"]), 'd/m/Y')?> al
                    <?=date_format(date_create_from_format('Y-m-d',$prenotazione["DataFine"]), 'd/m/Y')?></div>
                <?php } ?>
                <div><span uk-icon="location"> </span> <?=$prenotazione["NomeBiblioteca"]?></div>
            </div>
        </div>

        <h3>Eventi di consegna</h3>

        <?php
        $query = $db->prepare("CALL VISUALIZZA_CONSEGNE_DA_PRENOTAZIONE(:idPrenotazione);");
        $query->execute(array(
            ':idPrenotazione' => $prenotazione["Codice"]
        ));

        $presente = false;
        while($consegna = $query->fetch(PDO::FETCH_ASSOC)){
            $presente = true;
            echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            
            <h3 class="uk-margin-remove">'.$consegna["Tipo"].'</h3>
            <p>'.$consegna["Note"].'</p>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="calendar"></span>'.date_format(date_create_from_format('Y-m-d', $consegna["DataConsegna"]), 'd/m/Y').'</div>
                <div><span uk-icon="user"></span>'.$consegna["Nome"]." ".$consegna["Cognome"].'</div>
                <div><span uk-icon="sign-in"></span>'.$consegna["MezzoTrasporto"].'</div>
                </div>
        </div>';
            
        }
        if(!$presente){
            echo '<p>Non è ancora presente un evento di consegna associato alla prenotazione</p>';
        }

        ?>


    </div>
    <style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.prenotazioni-libri>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>
</body>
</html>
