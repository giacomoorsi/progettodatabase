<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Elenco messaggi</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco messaggi</h1>
        <?php if($_SESSION["StatoAccount"] == "Sospeso") echo '
    <div class="uk-alert uk-alert-danger"><h5>Account sospeso</h5><p>Il tuo account è stato sospeso per aver ricevuto 3 segnalazioni. <br>Non potrai più inserire prenotazioni. Puoi consultare le segnalazioni ricevute in <a href="elenco-segnalazioni.php">questa pagina</a>.</p></div>';
    ?>
    <?php


    $query = $db->prepare("CALL VISUALIZZA_MESSAGGI_UTENTE(:codiceUtilizzatore)");
    $query->execute(array(
    ':codiceUtilizzatore' => $_SESSION["Codice"]
    ));

    $presenti_messaggi = false;

    while($messaggio = $query->fetch(PDO::FETCH_ASSOC)){
        $presenti_messaggi = true;

        echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            
            <h3 class="uk-margin-remove">'.$messaggio["Titolo"].'</h3>
            <p>'.$messaggio["Testo"].'</p>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="calendar"></span>'.date_format(date_create_from_format('Y-m-d', $messaggio["Dataa"]), 'd/m/Y').'</div>
                <div><span uk-icon="user"></span>'.$messaggio["Nome"]." ".$messaggio["Cognome"].'</div>
            </div>
        </div>';
    
    }
    if(!$presenti_messaggi){
        echo '<p>Non hai ancora ricevuto messaggi.</p>';
    }
    ?>



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-messaggi>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
