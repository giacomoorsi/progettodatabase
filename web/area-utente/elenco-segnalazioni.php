<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Elenco segnalazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php';?>

    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco segnalazioni</h1>
        <?php if($_SESSION["StatoAccount"] == "Sospeso") echo '
    <div class="uk-alert uk-alert-danger"><h5>Account sospeso</h5><p>Il tuo account è stato sospeso per aver ricevuto 3 segnalazioni. <br>Non potrai più inserire prenotazioni. Puoi consultare le segnalazioni ricevute in <a href="elenco-segnalazioni.php">questa pagina</a>.</p></div>';
    ?>

    <?php


    $query = $db->prepare("CALL VISUALIZZA_SEGNALAZIONI_UTENTE(:codiceUtilizzatore)");
    $query->execute(array(
    ':codiceUtilizzatore' => $_SESSION["Codice"]
    ));

    $presente_segnalazione = false;
    while($segnalazione = $query->fetch(PDO::FETCH_ASSOC)){
        $presente_segnalazione = true;

        echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            <h3 class="uk-margin-remove uk-text-danger">Segnalazione</h3>
            <p>'.$segnalazione["Testo"].'</p>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="calendar"></span>'.$segnalazione["Dataa"].'</div>
                <div><span uk-icon="user"></span>'.$segnalazione["Nome"]." ".$segnalazione["Cognome"].'</div>
            </div>
        </div>';
        }
        if(!$presente_segnalazione){
           ?><p>Ottimo non hai ricevuto segnalazioni. Ti ricordiamo che la ricezione di 3 segnalazioni comporta la sospensione dell'account. Per maggiori informazioni consulta il <a target="_blank" href="https://sba.unibo.it/it/chi-siamo/organizzazione/fonti-normative/regolamenti">regolamento</a> delle biblioteche dell'Università di Bologna.</p> 
<?php
        } 


    ?>

</div>

</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-segnalazioni>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
