<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';

if(isset($_POST["biblioteca"])){
    $nomeBiblio = $_POST["biblioteca"];
} else {
    $nomeBiblio = "";
}

$query = $db->prepare("CALL VISUALIZZA_BIBLIOTECHE(:nome)");
$query->execute(array(
    ':nome' => $nomeBiblio
));

?>

<head>
    <?=get_head()?>
</head>

<body>
    <?php include 'template/header.php'; ?>
    <!--<div class="uk-section uk-section-muted uk-section-large uk-width">-->
    <div class="uk-container uk-background-muted uk-section uk-margin uk-container-xsmall">
        <h3>Ricerca biblioteca</h3>
        <form method="POST">
            <div uk-grid class="uk-flex-bottom">
                <div class="uk-width-1-2">
                    <label>Nome</label>
                    <input class="uk-input uk-width-1-1" name="biblioteca" value="<?=$nomeBiblio?>">
                </div>
                <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                    <button class="uk-button uk-button-primary" type="submit"  value="Ricerca">Ricerca</button>
                </div>
            </div>
        </form>
    </div>


    <div class="uk-container uk-section-large uk-container-xsmall">
        <!-- INZIO ELENCO BIBLIOTECHE -->


        <?php
            while($biblioteca = $query->fetch(PDO::FETCH_ASSOC)) { ?>
        <div class="uk-card uk-margin uk-card-default uk-card-body">
            <div uk-grid class="uk-margin">
                <div>
                    <h3 class="uk-margin-remove"><?=$biblioteca["Nome"]?></h3>
                </div>
                <div class="uk-width-expand uk-text-right"><a href="/biblioteca.php?nome=<?=urlencode($biblioteca["Nome"])?>"
                        class="uk-button uk-button-text">Apri scheda</a></div>
            </div>
            <div class="uk-grid uk-margin-small">
                <div><span uk-icon="location"> </span> <?=$biblioteca["Indirizzo"]?></div>
                <div><span uk-icon="info"> </span> <?=$biblioteca["PostiLettura"]?> posti lettura</div>
                <div><span uk-icon="file-text"> </span> <?=$biblioteca["Cartacei"]?> libri cartacei</div>
                <div><span uk-icon="tablet"> </span> <?=$biblioteca["Ebook"]?> E-Book</div>
            </div>
        </div>
        <?php } ?>

    </div>


    <body>

</html>