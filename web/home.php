<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';
?>

<head>
    <title>e-Biblio</title>
    <?=get_head()?>
</head>

<body>
    <?php include 'template/header.php'; ?>
    <!--<div class="uk-section uk-section-muted uk-section-large uk-width">-->
    <div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport" uk-height-viewport="offset-top: true">
        <div uk-grid class="uk-width-xxlarge uk-child-width-1-3">
            <div>
                <a href="/biblioteche.php">
                    <div class="uk-card uk-card-body uk-card-hover uk-card-default uk-text-center">
                        <span uk-icon="icon:location; ratio:3"></span>
                        <h2>Biblioteche</h2>
                    </div>
                </a>
            </div>
            <div>
                <a href="/ricerca-libri.php">
                    <div class="uk-card uk-card-body uk-card-hover uk-card-default uk-text-center">
                        <span uk-icon="icon:search; ratio:3"></span>
                        <h2>Ricerca libri</h2>
                    </div>
                </a>
            </div>
            <div>
                <a href="/statistiche.php">
                    <div class="uk-card uk-card-body uk-card-hover uk-card-default uk-text-center">
                        <span uk-icon="icon:database; ratio:3"></span>
                        <h2>Statistiche</h2>
                    </div>
                </a>
            </div>
        </div>
    </div>
</body>

</html>