<?php
$e = $_GET["e"];
$e_msg = ["Username o password non validi"];
require 'utilities/functions.php';

if (isset($_POST["user"]) && $_POST["password"]){
	// Eseguo LogIn
	$query = $db->prepare("CALL VISUALIZZA_UTENTE (:user )");
	$query->execute(array(
		':user' => $_POST["user"]
	));


	while ($user = $query->fetch(PDO::FETCH_ASSOC)) {
		if(password_verify($_POST["password"], $user['Password'])){
			// login autorizzato
			
			// inserisco tutte le varibili nell'utente nella variabile di sessione

			foreach($user as $k => $v){
				$_SESSION[$k] = $v;
			}

			inviaLog("Login effettuato : " . $_POST["user"]);

			switch ($_SESSION["TipoUtente"]) {
					case "Amministratore" : header("Location: /area-amministratore/"); break;
					case "Utilizzatore" : header("Location: /area-utente/"); break;
					case "Volontario" : header("Location: /area-volontario/"); break;
			}
			die(0);
		}
	}

	header("Location: ?e=0");
	die(0);




}


?>

<!DOCTYPE html>
<html>
<head>
  <?=get_head();?>
</head>
<body>
      <?php include 'template/header.php'; ?>
    
    <div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport" uk-height-viewport="offset-top: true">
			<!--<div class="uk-position-bottom-center uk-position-small uk-visible uk-text-center">
				<span class="uk-text-small uk-text-muted uk-text-center"><a href="/admin/attiva-utente/">Attiva credenziali</a><br>versione 1.0.0</span>
			</div>-->
			<div class="uk-width-large uk-padding-large uk-background-muted">
				<form method="POST">
                    <h4>Effettua il login</h4>
					<fieldset class="uk-fieldset">
						<!--<legend class="uk-legend">Login</legend>-->

						<div class="uk-margin">
							<div class="uk-inline uk-width-1-1">
								<span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="user"></span>
								<input class="uk-input uk-form-large" name="user" required="" placeholder="Email" type="text">
							</div>
						</div>
						<div class="uk-margin">
							<div class="uk-inline uk-width-1-1">
								<span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="lock"></span>
								<input class="uk-input uk-form-large" name="password" required="" placeholder="Password" type="password">
							</div>
						</div>
						
						<!--<div class="uk-margin">
							<label><input class="uk-checkbox" type="checkbox" name="keep"> Ricordami</label>
						</div>-->
					<?php
						echo (isset($_GET["e"])) ? '<p class="uk-text-danger">'.$e_msg[$e].'</p>' : "";
					?>
						<div class="uk-margin">
							<button type="submit" class="uk-button uk-button-primary uk-button-primary uk-button-large uk-width-1-1">Login</button>
						</div>
					</fieldset>
				</form>
				<div uk-grid class="uk-flex-middle">
					<div><h6>Non sei registrato? </h6> </div>
					<div><a href="/registrazione.php" class="uk-button uk-button-text">Registrati</a></div>
				</div>
				<p><a target="_blank" href="credenziali.txt" class="uk-button uk-button-text">Credenziali accesso</a></p>
			</div>
		</div>
    

</body>
</html>