<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';
?>
<head>
  <?=get_head()?>
</head>
<body>
  <?php include 'template/header.php'; ?>
    <!--<div class="uk-section uk-section-muted uk-section-large uk-width">-->
      <div class="uk-container uk-background-muted uk-section uk-margin uk-container-xsmall"> 
            <h3>Ricerca libro</h3>

            
          <form method="POST">
              <div uk-grid class="uk-flex-bottom">
                  <div class="uk-width-1-2">
                      <label>Titolo</label>
                      <input name="titolo" class="uk-input uk-width-1-1" value="<?= isset($_POST["titolo"]) ? $_POST["titolo"] : ""; ?>">
                  </div>
                  <div class="uk-width-1-4">
                      <label>Autore</label>
                      <input placeholder="Inserisci il cognome" name="autore" class="uk-input uk-width-1-1" value="<?= isset($_POST["autore"]) ? $_POST["autore"] : ""; ?>">
                  </div>
                  <div class="uk-width-1-4">
                      <label>Anno di pubblicazione</label>
                      <input name="anno" type="number" class="uk-input uk-width-1-1" value="<?= isset($_POST["anno"]) ? $_POST["anno"] : ""; ?>">
                  </div>
                  <div class="uk-width-1-4">
                      <label>Tipo libro</label>
                      <select name="tipo" class="uk-input uk-select">
                        <option value="Qualsiasi">Qualsiasi</option>
                        <option value="Ebook" <?= (isset($_POST["tipo"]) && $_POST["tipo"]=="Ebook") ? "selected" : ""; ?>>E-book</option>
                        <option value="Cartaceo" <?= (isset($_POST["tipo"]) && $_POST["tipo"]=="Cartaceo") ? "selected" : ""; ?>>Libro Cartaceo</option>
                      </select>
                  </div>
                  <div class="uk-width-1-4">
                      <label>Codice libro</label>
                      <input name="codice" type="number" class="uk-input uk-width-1-1" value="<?= isset($_POST["codice"]) ? $_POST["codice"] : ""; ?>">
                  </div>
                  <div class="uk-width-1-4">
                      <label>Biblioteca</label>
                      <select name="biblioteca" class="uk-input uk-select">
                        <option value="Qualsiasi">Qualsiasi</option>
                        <?php
                        $query = $db->prepare('CALL VISUALIZZA_BIBLIOTECHE("")');
                        $query->execute();

                        while($biblioteca = $query->fetch(PDO::FETCH_ASSOC)){
                            echo '<option value="' . $biblioteca["Nome"] . '"';
                            if(isset($_POST["biblioteca"]) && $_POST["biblioteca"] == $biblioteca["Nome"]){
                                echo 'selected ';
                            }
                            echo '>'. $biblioteca["Nome"]. '</option>';
                        }
                        ?>
                      </select>
                  </div>
                  <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                      <button class="uk-button uk-button-primary" type="submit" value="Ricerca">Ricerca</button>
                  </div>
              </div>
          </form>
        </div>
    <!-- Risultati di ricerca -->
    
    <div class="uk-container uk-section-large uk-container-xsmall">
        <!-- INZIO ELENCO LIBRI -->
        <?php
        if(isset($_POST["titolo"])){
            $titolo = $_POST["titolo"];
        } else $titolo = "";

        if(isset($_POST["autore"])){
            $autore = $_POST["autore"];
        } else $autore = "";

        if(isset($_POST["codice"])){
            $codice = $_POST["codice"];
        } else $codice = 0;

        if(isset($_POST["anno"])){
            $anno = $_POST["anno"];
        } else $anno = 0;

        if(isset($_POST["biblioteca"])){
            if($_POST["biblioteca"] == "Qualsiasi"){
                $biblioteca = "";
            } else $biblioteca = $_POST["biblioteca"];
        } else $biblioteca = "";

        // Link dalla pagina della biblioteca
        if(isset($_GET["biblioteca"])){
            $biblioteca = $_GET["biblioteca"];
        }

        if(isset($_POST["tipo"])){
            if($_POST["tipo"] == "Qualsiasi"){
                $tipo = "";
            } else $tipo = $_POST["tipo"];
        } else $tipo = "";

        $query = $db->prepare("CALL FILTRA_LIBRI(:codLibro, :titolo, :autore, :annoPubbl, :tipoLibro, :biblioteca);");
	    
        
        $query->execute(array(
            ':codLibro' => $codice,
            ':titolo' => $titolo,
            ':autore' => $autore,
            ':annoPubbl' => $anno,
            ':tipoLibro' => $tipo,
            ':biblioteca' => $biblioteca
        ));
        $libri = $query->fetchAll(PDO::FETCH_ASSOC);
        $query->closeCursor();

        $presente = false;

        foreach($libri as $libro){
            $presente = true;

        echo '<div class="uk-card uk-margin uk-card-default uk-card-body">';
        $disponibile = true;
        if($libro["Tipo"] == "Cartaceo"){
            if( $libro["StatoPrestito"] == "Disponibile"  && $libro["StatoConservazione"] != "Scadente" ) {
                echo '<div class="uk-card-badge uk-badge-success uk-label">Disponibile</div>';
            }
            else {
                echo '<div class="uk-card-badge uk-badge-danger uk-label">Non Disponibile</div>';
                $disponibile = false;
            }
        }
        if($libro["Tipo"] == "Ebook"){
            echo '<div class="uk-card-badge uk-badge-success uk-label">Online</div>';
        }

        echo '<div>
                    <h4 class="uk-text-muted uk-margin-remove">'. listaAutori($db, $libro["Codice"]).'</h4>
                    <h3 class="uk-margin-remove">' . $libro["Titolo"] .  '</h3>
                </div>
            <div class="uk-grid uk-margin-small">
                <div><span uk-icon="location"> </span>' . $libro["NomeBiblioteca"] .  '</div>
                <div><span uk-icon="file-text"></span>' . $libro["Tipo"] .  '</div>
                <div><span uk-icon="bookmark"></span>' . $libro["Genere"] .  '</div>';

                if($disponibile) echo '<div class="uk-expand uk-text-right"><a href="/libro.php?id='.$libro["Codice"].'" class="uk-button uk-button-text">Apri scheda</a></div>';
         echo '</div></div>';

        }

        if(!$presente){
            echo '<p class="uk-text-large">Siamo spiacenti, non è stato trovato nessun libro con il filtro impostato</p>';
        }
    ?>
        
        <!-- INIZIO PAGINATION 
          <ul class="uk-pagination uk-margin-large uk-flex-center" uk-margin>
            <li><a href="#"><span uk-pagination-previous></span></a></li>
            <li><a href="#">1</a></li>
            <li class="uk-disabled"><span>...</span></li>
            <li><a href="#">6</a></li>
            <li class="uk-active"><span>7</span></li>
            <li><a href="#">8</a></li>
            <li><a href="#"><span uk-pagination-next></span></a></li>
        </ul>
        FINE PAGINATION -->       
    </div>
</body>
</html>