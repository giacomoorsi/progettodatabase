<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';

if(!isset($_SESSION["TipoUtente"]) || $_SESSION["TipoUtente"] != "Utilizzatore" ){
    header("Location: /login.php");
    die(0);
}


?>

<head>
    <?=get_head()?>
</head>

<body>
    <?php include 'template/header.php'; 

    if(!isset($_SESSION["TipoUtente"]) || $_SESSION["TipoUtente"] != "Utilizzatore"){
        header('Location: login.php');
    }

    $biblioteca = $_GET["biblioteca"];
    $posto = $_GET["posto"];
    $data = $_GET["data"];
    $orario_inizio = $_GET["orario_inizio"];
    $orario_fine = $_GET["orario_fine"];
    $ethernet = $_GET["ethernet"];
    $elettrico = $_GET["elettrico"];

    $data_leggibile = date_format(date_create_from_format('Y-m-d', $data), 'd/m/Y');


    if(isset($_GET["conferma"])){
        // Inserisco nel database
        $query = $db->prepare("CALL PRENOTA_POSTO_LETTURA(:utente, :numero, :biblioteca, :data, :orainizio, :orafine);");

        $query->execute(array(
            ':utente' => $_SESSION["Codice"],
            ':numero' => $posto,
            ':biblioteca' => $biblioteca,
            ':data' => $data,
            ':orainizio' => $orario_inizio,
            ':orafine' => $orario_fine
        ));
        inviaLog("Codice Utente :".$_SESSION["Codice"]." - Operazione : Prenotazione Posto Lettura - Biblioteca : ".$biblioteca." - numero posto : ".$posto );

        header('Location: /area-utente/prenotazioni-lettura.php');

    }

    ?>

    <div class="uk-container uk-section-large uk-container-small">
        <div class="uk-card uk-card-body uk-card-default">
            <div class="uk-expand uk-text-left"><button onclick="window.history.back();"
                    class="uk-button uk-button"><span uk-icon="arrow-left"></span> Torna indietro</button></div>
            <div uk-grid class="uk-margin">
                <div>
                    <h2 class="uk-margin-remove">Prenotazione posto n. <?=$posto ?>
                        <span uk-icon="bolt"
                            class="<?php echo  $ethernet ? 'uk-text-success' : 'uk-text-danger';?>"></span>
                        <span uk-icon="link"
                            class="<?php echo  $elettrico ? 'uk-text-success' : 'uk-text-danger';?>"></span>
                    </h2>
                </div>
                <div class="uk-text-right uk-width-expand">
                    <a class="uk-button uk-button-secondary uk-button-large"
                        href="prenotazione-lettura-conferma.php?biblioteca=<?=urlencode($biblioteca)?>&posto=<?=$posto?>&data=<?=urlencode($data)?>&orario_inizio=<?=urlencode($orario_inizio)?>&orario_fine=<?=urlencode($orario_fine)?>&ethernet=<?= $ethernet ? "1" : "0"?>&elettrico=<?= $elettrico ? "1" : "0"?>&conferma=1">Conferma
                    </a>
                </div>
            </div>
            <div class="uk-grid uk-margin-small">
                <div><span uk-icon="location"> </span> <?=$biblioteca?></div>
                <div><span uk-icon="calendar"></span> <?=$data_leggibile?></div>
                <div><span uk-icon="clock"></span> <?=$orario_inizio?> - <?=$orario_fine?> </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>