<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$inviareLog = false;

if($inviareLog){
    require __DIR__ . '/../vendor/autoload.php';
}

include 'head.php';


session_start();

// MySQL 
$dbname = "sql11407266";
$dbuser = "sql11407266";
$dbpassword = "P2VynsE4RA";
$dbhost = "sql11.freemysqlhosting.net";
$db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpassword);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);




function listaAutori($db, $codicelibro){
    $query = $db->prepare("CALL VISUALIZZA_AUTORI(:codicelibro);", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
    $query->execute(array(
        ':codicelibro' => $codicelibro
    ));
    $output = "";
    $numero = 0;
    while($autore = $query->fetch(PDO::FETCH_ASSOC)){
        if ($numero==0) {
            $output .= substr($autore["Nome"], 0, 1) . '. ' . $autore["Cognome"];
        }
        else {
            $output .= ', ' . substr($autore["Nome"], 0, 1) . '. ' . $autore["Cognome"];
        }
        $numero++; 
    }
    return $output;
}


function inviaLog($log){
    $inviareLog = false;
    if($inviareLog){
        try {
            // MongoDB
            $client = (new MongoDB\Client(  'mongodb://localhost:27017/Ebiblio?retryWrites=true&w=majority'));
            $collection = $client->selectCollection('Ebiblio', 'log');
            date_default_timezone_set("Europe/Rome");
            $insertOneResult = $collection->insertOne([
                'log' => $log,
                'time' => date("d-m-Y H:i:s")
            ]);
        } catch(Exception $e){
            echo '<script>console.log("Errore invio log: '. $e->getMessage() . '")</script>';
        }
    }
}

?>