<?php
/*
function get_head() {
    return '<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.17/dist/css/uikit.min.css" />
    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.17/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.17/dist/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="/template/custom.css" />
    ';

}*/

function get_head() {
    return '<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- UIkit CSS -->
    <link rel="stylesheet" href="/template/uikit/theme.css" />
    <!-- UIkit JS -->
    <script src="/template/uikit/theme.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.17/dist/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="/template/custom.css" />
    <script   src="https://code.jquery.com/jquery-3.6.0.min.js"   integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="   crossorigin="anonymous"></script>
    ';
    
}