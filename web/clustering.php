<?php
use Phpml\Clustering\KMeans;
require 'utilities/functions.php';

require 'vendor/autoload.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>e-Biblio</title>
    <?=get_head()?>
</head>
<body>

<?php

/*
Implementare un sistema di clustering basato su algoritmo di K-Means, attraverso il quale si segmentano gli utenti utilizzatori, sulla base della loro professione, età, genere e numero di richieste di prestiti di libri cartacei effettuati. Visualizzare -tramite apposita funzionalità nella piattaforma- l’elenco degli utenti che appartiene a ciascun cluster.
*/

if(isset($_GET["genera"])){

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);

    $query = $db->prepare("CALL VISUALIZZA_DATI_CLUSTERING();");
    $query->execute();

    $utilizzatori = [];
    $professioni = [];
    //inseriemento dati nell'array
    while($utente=$query->fetch(PDO::FETCH_ASSOC)){
        $eta = date('Y') - date_format(date_create_from_format('Y-m-d', $utente["DataNascita"]), 'Y'    );
        if(in_array($utente["Professione"], $professioni)){
            $professione = array_search($utente["Professione"], $professioni);
        } else {
            array_push($array, $utente["Professione"]);
        }
        array_push($utilizzatori, [$utente['Email'],  $eta, $professione, $utente["NPrenotazioni"]]);
    }


    //esecuzione algoritmo kmeans
    $clusterer = new KMeans(3);
    $clusters = $clusterer->cluster($utilizzatori);

    //stampa dei risultati 
    $output = [];
    foreach ($clusters as $key => $cluster) {
        foreach ($cluster as $sample) {
            $output[] = sprintf('%s;%s', $key, $sample[0]);
        }
    }
    //inserimento dei dati all'interno del file .csv
    file_put_contents('cluster/output.csv', implode(PHP_EOL, $output));
    echo '<script>UIkit.modal.alert("Il clustering è completato. Premere su `Visualizza clusters` per visualizzare i risultati").then();</script>';
}


?>

    <?php include 'template/header.php'; ?>
    <!--<div class="uk-section uk-section-muted uk-section-large uk-width">-->
    <div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport" uk-height-viewport="offset-top: true">
        <div class="uk-card uk-card-default uk-card-body">
        <h1>Clustering</h1>
        <p>Da questa pagina è possibile clusterizzare gli utilizzatori presenti nel database di eBiblio.</p>
        <p>L'output è memorizzato in un file in formato csv</p>
        <div uk-grid>
        <div><a href="?genera=1" class="uk-button uk-button-primary">Avvia clustering</a></div>
        <div><a href="/cluster/output.csv" class="uk-button uk-button-secondary">Visualizza clusters</a></div>
        </div>
    </div>
</body>

</html>

