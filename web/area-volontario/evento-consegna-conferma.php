<!DOCTYPE html>
<html>
<?php 
require '../utilities/functions.php';
if($_GET["tipo"]== "A") $tipo = "Affidamento";  
else if ($_GET["tipo"]== "R") $tipo = "Restituzione";

if(isset($_POST["note"])){
    $query = $db->prepare("CALL INSERISCI_CONSEGNA(:tipo, :codicePrenotazione, :note, :codiceVolontario, :data);");
    $query->execute(array(
        ':tipo' => $tipo,
        ':codicePrenotazione' => $_GET["id"],
        ':note' => $_POST["note"],
        ':codiceVolontario' => $_SESSION["Codice"],
        ':data' => date ("Y-m-d")
    ));
    header("Location: /area-volontario/");
}






$query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONE(:codice)");
    $query->execute(array(
        ':codice' => $_GET["id"]
    ));
    inviaLog("Codice Volontario :".$_SESSION["Codice"]." - Operazione : Inserita Consegna - id Codice Prenotazione : ".$_GET["id"]);
?>

<head>
    <?=get_head()?>
</head>

<body>
    <?php include '../template/header.php';   

$prenotazione = $query->fetch(PDO::FETCH_ASSOC);

echo '<div class="uk-container uk-section-large uk-container-small">
    <div class="uk-expand uk-text-left"><button onclick="window.history.back();" class="uk-button uk-button"><span uk-icon="arrow-left"></span> Torna indietro</button></div>
        <div class="uk-card uk-margin uk-card-default uk-card-body">
                    <div uk-grid class="uk-margin">
                        <div><h2 class="uk-margin-remove">Inserimento evento di '.$tipo.'</h2></div>
                    </div>
                <div class="uk-grid-small uk-margin-small" uk-grid>
                    <div><span uk-icon="calendar"></span>'.date ("d/m/Y").'</div>
                    <div><span uk-icon="location"></span>'.$prenotazione["NomeBiblioteca"].'</div>
                    <div><span uk-icon="user"></span>'.$prenotazione["Nome"]." ".$prenotazione["Cognome"].'</div>
                    <div><span uk-icon="home"></span>'.$prenotazione["Indirizzo"]." ".$prenotazione["Citta"].'</div>
                    <div><span uk-icon="file-text"></span>'.$prenotazione["Titolo"].'</div>
                </div>';





                ?>
            <form method="POST" class="uk-margin-medium">
            <div >
                 
                <div>
                    <h4>Note</h4>
                    <textarea class="uk-textarea uk-width-1-1"  rows="5" name="note" ></textarea>
                </div>
                <div class="uk-margin uk-text-center">
                <input type="submit" value="Conferma" class="uk-button uk-button-secondary">
                <input type="hidden" name="idprenotazione" value="<?=$_GET["id"]?>">
                </div>
            </form>

        </div>
    </div>
</body>

</html>