<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
    
<head>
    <title>Eventi di consegna</title>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Eventi di consegna</h1>

        <?php


    $query = $db->prepare("CALL VISUALIZZA_CONSEGNE_DA_VOLONTARIO(:idVolontarioIN)");
    $query->execute(array(
    ':idVolontarioIN' => $_SESSION["Codice"]
    ));

    $presenti_consegne = false;

    while($consegna = $query->fetch(PDO::FETCH_ASSOC)){
        $presenti_consegne = true;

        echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            
            <h3 class="uk-margin-remove">'.$consegna["Titolo"].'</h3>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="location"></span>'.$consegna["NomeBiblioteca"].'</div>
                <div><span uk-icon="calendar"></span>'.date_format(date_create_from_format('Y-m-d', $consegna["DataConsegna"]), 'd/m/Y').'</div>
                <div><span uk-icon="user"></span>'.$consegna["Nome"]." ".$consegna["Cognome"].'</div>
                <div><span uk-icon="home"></span>'.$consegna["Indirizzo"]." ".$consegna["Citta"].'</div>
                <div><span uk-icon="info"></span>'.$consegna["Tipo"].'</div>
                </div>
        </div>';
    
    }
    if(!$presenti_consegne){
        echo '<p>Non hai ancora registrato consegne.</p>';
    }
    ?>



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-messaggi>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
