<!DOCTYPE html>
<html>
<?php 
require '../utilities/functions.php';
$query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONE(:codice)", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
    $query->execute(array(
        ':codice' => $_GET["id"]
    ));
?>
<title>Aggiungi evento di Consegna</title>
<head>
    <?=get_head()?>
</head>

<body>
    <?php include '../template/header.php'; ?>


    <div class="uk-container uk-padding ">
        <div class="uk-expand uk-text-left"><button onclick="window.history.back();" class="uk-button uk-button"><span
                    uk-icon="arrow-left"></span> Torna indietro</button></div>
        <div class=" uk-card uk-card-default uk-card-body uk-margin">


            <?php
$prenotazione = $query->fetchAll(PDO::FETCH_ASSOC)[0];
$query->closeCursor();

         echo  '<div uk-grid class="uk-flex-bottom">
                    <div>
                        <h1>'.$prenotazione["Titolo"].'</h1>
                    </div>
                </div>

            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="calendar"></span>'.date ("d/m/Y").'</div>
                <div><span uk-icon="user"></span>'.$prenotazione["Nome"]." ".$prenotazione["Cognome"].'</div>
                <div><span uk-icon="home"></span>'.$prenotazione["Indirizzo"]." ".$prenotazione["Citta"].'</div>
            </div>

        </div>';

        ?>

    </div>

    <div class="uk-margin-large uk-text-center">
    <?php
    if($_GET["tipo"] == 'A') {
    ?>
        <a href="evento-consegna-conferma.php?id=<?=$prenotazione["Codice"]?>&tipo=A" class="uk-button uk-button-secondary uk-button-large">Crea evento di consegna</a>
    <?php
    }else if($_GET["tipo"] == 'R') {
    ?>
        <a href="evento-consegna-conferma.php?id=<?=$prenotazione["Codice"]?>&tipo=R" class="uk-button uk-button-secondary uk-button-large">Crea evento di restituzione</a>
    <?php
    }
    ?>
    </div>

</body>

</html>