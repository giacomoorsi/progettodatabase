<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Elenco prenotazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco prenotazioni</h1>

<!--
        <div class="uk-card uk-card-default uk-card-body ">
            <h3>Filtra prenotazioni</h3>
            <form action="elenco-prenotazioni-volontario.php" method="POST">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Codice libro</label>
                        <input name="codiceLib" type="number" class="uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-2">
                        <label>Codice utilizzatore</label>
                        <input name="codiceUt"type="number" class="uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-2">
                        <label>Data inizio filtro</label>
                        <input name="dataI" type="text" class="uk-input uk-width-1-1" placeholder="gg/mm/aaaa" required  >
                    </div>
                    <div class="uk-width-1-2">
                        <label>Data fine filtro</label>
                        <input name="dataF"type="text" class="uk-input uk-width-1-1" placeholder="gg/mm/aaaa" required >
                    </div>
                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button uk-button-" type="submit" value="Ricerca">Ricerca</button>
                    </div>
                </div>
            </form>
        </div>-->
        
        <?php

        $query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONI_VOLONTARIO()");
        $query->execute();

        $presente = false;
        while($prenotazione = $query->fetch(PDO::FETCH_ASSOC)) {
            $presente = true;
    
            if($prenotazione["StatoPrestito"]=="Prenotato"){
                $stato = "Da consegnare";
            } else if ($prenotazione["StatoPrestito"]=="Consegnato"){
                $stato = "Da ritirare";
            } else {
                $stato = "errore";
            }


                   ?>

                    <div class="uk-card uk-margin uk-card-default uk-card-body">
                        <div class="uk-card-badge uk-label"><?=$stato?></div>
                        <div uk-grid>
                            <div class="uk-width-expand">
                                <h3 class="uk-margin-remove"><?=$prenotazione["Titolo"]?></h3>
                            </div>
                            <div class="uk-width-auto uk-text-right">
                            </div>
                        </div>
                        <div class="uk-grid uk-margin-small" uk-grid>
                            <div><span uk-icon="location"> </span> <?=$prenotazione["NomeBiblioteca"]?></div>
                            <div><span uk-icon="user"> </span> <?=$prenotazione["NomeU"]?>  <?=$prenotazione["CognomeU"]?></div>
                            <div><span uk-icon="home"> </span><?=$prenotazione["Indirizzo"]?>, <?=$prenotazione["Citta"]?></div>
                            <div><span uk-icon="hashtag"> </span>Prenotazione n. <?=$prenotazione["Codice"]?></div>
                            <?php if($stato == "Da ritirare"){
                                echo '<div><span uk-icon="calendar"> </span>' .date_format(date_create_from_format('Y-m-d',$prenotazione["DataFine"]), 'd/m/Y') . '</div>';
                                echo '<div class="uk-expand uk-text-right"><a href="/area-volontario/evento-consegna-conferma.php?id=';
                                echo $prenotazione["Codice"];
                                echo '&tipo=R';
                                echo '" class="uk-button uk-button-text">Inserisci evento di ritiro</a></div>';
                            } else if($stato == "Da consegnare"){
                                echo '<div class="uk-expand uk-text-right"><a href="/area-volontario/evento-consegna-conferma.php?id=';
                                echo $prenotazione["Codice"];
                                echo '&tipo=A';
                                echo '" class="uk-button uk-button-text">Inserisci evento di affidamento</a></div>';
                            } ?>



                            
                        </div>
                    </div>
                    <?php }
        if(!$presente){
            echo "<p>Ottimo! Non c'è nessuna prenotazione da consegnare o da ritirare.</p>";
        }
        ?>
        
    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-prenotazioni>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>