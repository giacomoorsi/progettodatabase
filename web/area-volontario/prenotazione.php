<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
$query = $db->prepare("CALL VISUALIZZA_PRENOTAZIONE(:codice)", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
$query->execute(array(
        ':codice' => $_GET["id"]
    ));
?>
<title>Prenotazione</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>

<?php


$prenotazione = $query->fetchAll(PDO::FETCH_ASSOC)[0];
$query->closeCursor();

    echo '<div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Prenotazione #'.$prenotazione["Codice"].'</h1>

        <div class="uk-container uk-padding ">
        <div class="uk-expand uk-text-left"><button onclick="window.history.back();" class="uk-button uk-button"><span
        uk-icon="arrow-left"></span> Torna indietro</button></div>

            <div class="uk-card uk-margin uk-card-default uk-card-body">

                <h2 class="uk-margin-remove">'.$prenotazione["Titolo"].'</h3>
                <div class="uk-grid-small uk-margin-small" uk-grid>
                    <div><span uk-icon="calendar"></span>'.$prenotazione["DataInizio"]." - ".$prenotazione["DataFine"].'</div>
                    <div class="uk-expand uk-text-right"><a href="/libro.php?id='.$prenotazione["CodiceLibro"].'" class="uk-button uk-button-text">Apri scheda libro</a></div>
                </div>
            </div>';
?>    

        </div>
</div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.prenotazioni-libri>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
