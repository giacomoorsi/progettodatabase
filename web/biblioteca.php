<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';

if(!isset($_GET["nome"])){
    header("Location: biblioteche.php");
    die(0);
}

$query = $db->prepare("CALL VISUALIZZA_BIBLIOTECA(:nome);");
$query->execute(array(
    ':nome' => $_GET["nome"]
));

if(!$biblioteca = $query->fetch(PDO::FETCH_ASSOC)){
    header("Location: biblioteche.php");
    die(0);
};


/*$biblioteca = [
    "Nome" => "Palazzo Paleotti",
    "Latitudine" => 44.49644868647701,
    "Longitudine" =>  11.35111790280143,
    "NoteStoriche" => 1234,
    "SitoWeb" => "https://unibo.it",
    "Indirizzo" => "Via Zamboni 7, Bologna",
    "NumeroPosti" => 145,
    "NumeroLibriCartacei" => 1730,
    "NumeroEbook" => 90 
];*/


//$biblioteca["NoteStoriche"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tincidunt ultrices volutpat. Duis ligula sem, luctus ut tincidunt id, semper nec erat. In tristique suscipit ultrices. Aenean in lectus luctus, tristique enim id, volutpat velit. Suspendisse volutpat, dolor ac dignissim volutpat, felis elit pharetra nulla, vitae iaculis felis felis in diam. Ut sodales commodo justo sed finibus. Donec at imperdiet nisi. Duis felis justo, volutpat ac lectus ac, placerat euismod leo. Duis arcu mi, pulvinar ut quam et, tincidunt rutrum lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam ac felis pulvinar, tempus metus non, scelerisque lectus. Sed quis mi et sem porta egestas.";
// $id = $_GET["id"];
?>

<head>
    <title>Biblioteca <?=$biblioteca["Nome"]?></title>
    <?=get_head()?>

    <!-- FILE PER LA MAPPA -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
</head>

<body>
    <?php include 'template/header.php'; ?>

    <div class="uk-container uk-padding">
    <div class="uk-expand uk-text-left"><a href="/biblioteche.php" class="uk-button uk-button"><span uk-icon="arrow-left"></span> Torna indietro</a></div>
    <div class=" uk-card uk-card-default uk-card-body uk-margin">

        <h1><?=$biblioteca["Nome"]?></h1>
        <div uk-grid>
            <div><span uk-icon="location"></span> <?=$biblioteca["Indirizzo"]?></div>
            <div><span uk-icon="world"></span> <a href="<?=$biblioteca["SitoWeb"]?>" class="uk-button uk-button-text"
                    target="_blank">Sito web</a></div>

            <div><span uk-icon="info"></span> <?=$biblioteca["PostiLettura"]?> posti lettura</div>
            <div><span uk-icon="file-text"></span> <?=$biblioteca["Cartacei"]?> libri cartacei</div>
            <div><span uk-icon="tablet"></span> <?=$biblioteca["Ebook"]?> E-Book</div>
            <?php 
                $query = $db->prepare("CALL VISUALIZZA_RECAPITI(:nome);");
                $query->execute(array(
                    ':nome' => $biblioteca["Nome"]
                ));
                while($recapito = $query->fetch(PDO::FETCH_ASSOC)){
                    echo '<div><span uk-icon="receiver"></span> ' . $recapito["Numero"] . '</div>';   
                }

            ?>
        </div>

        <div uk-grid class="uk-child-width-1-2 uk-flex-middle">
            <div>
                <div id="map" style="height:500px"></div>

                <script>
                    var map = L.map('map').setView([ <?=$biblioteca["Latitudine"]?> , <?= $biblioteca["Longitudine"]?> ], 14);


                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(map);

                    L.marker([ <?=$biblioteca["Latitudine"] ?> , <?=$biblioteca["Longitudine"] ?> ]).addTo(map).bindPopup('<b><?=$biblioteca["Nome"]?></b><br><?=$biblioteca["Indirizzo"]?>').openPopup();
                </script>
            </div>
            <div>
                <h3>Note storiche</h3>
                <p><?=$biblioteca["NoteStoriche"]?></p>

                <a href="ricerca-libri.php?biblioteca=<?=urlencode($biblioteca["Nome"])?>" class="uk-button uk-button-primary">Catalogo
                    libri</a>

                    <div class="uk-margin">
                        <div class="uk-child-width-1-3@m" uk-grid uk-lightbox="animation: slide">
                            <?php
                            $query = $db->prepare('CALL VISUALIZZA_IMMAGINI_BIBLIOTECA(:nome);');
                            $query->execute(array(
                                ':nome' => $biblioteca["Nome"]
                            ));

                            while($immagine = $query->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <div>
                                <a class="uk-inline" href="/img/<?=$immagine["Nome"]?>">
                                    <img src="/img/<?=$immagine["Nome"]?>" alt="">
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
            </div>

        </div>
        <hr />

        <h3 id="posti-lettura">Posti lettura</h3>
        <p>È possibile prenotare un posto lettura presso la biblioteca.</p>
        <?php if(isset($_SESSION["StatoAccount"]) && $_SESSION["StatoAccount"] == "Sospeso") echo '
    <div class="uk-alert uk-alert-danger"><h5>Account sospeso</h5><p>Il tuo account è stato sospeso per aver ricevuto 3 segnalazioni. <br>Non potrai più inserire prenotazioni. Puoi consultare le segnalazioni ricevute in <a href="/area-utente/elenco-segnalazioni.php">questa pagina</a>.</p></div><style>#form-posti-lettura, #elenco-posti-disponibili{display:none}</style>';
    ?>


        <?php


            if(!isset($_GET["data"])){
            
                $data = date_create_from_format('Y-m-d', date('Y-m-d'));
                $orario_inizio = "09:00";
                $orario_fine = "11:00";
                $data_leggibile = date_format($data, 'd/m/Y');
            } else {
                $data = $_GET["data"];
                $orario_inizio = $_GET["orario_inizio"];
                $orario_fine = $_GET["orario_fine"];
                $data = date_create_from_format('d/m/Y', $data);
                if(!$data || $data<date_create_from_format('Y-m-d', date('Y-m-d'))){
                    echo '<p class="uk-text-danger">La data inserita non è valida</p>';
                    $data = date_create_from_format('Y-m-d', date('Y-m-d'));
                    $data_leggibile = date_format($data, 'd/m/Y');
                }else {
                    $data_leggibile = date_format($data, 'd/m/Y');
                }

                if((substr($orario_inizio, 0, 2) > substr($orario_fine, 0, 2)) || (substr($orario_inizio, 0, 2) == substr($orario_fine, 0, 2) && substr($orario_inizio, -2) >= substr($orario_fine, -2) ) ) {
                    echo '<p class="uk-text-danger">L`orario inserito non è valido</p>';
                    $orario_inizio = "09:00";
                    $orario_fine = "11:00";
                } 


            }




            

        ?>
        <form id="form-posti-lettura" method="GET" action="biblioteca.php#posti-lettura">
            <div uk-grid class="uk-margin uk-flex-middle">
                <div class="uk-grid-small" uk-grid>

                    <?php /*
                    <div>
                        <label>Giorno</label>
                        <select name="giorno" class="uk-select">
                            <?php
                            for($i=1; $i<=31; $i++){
                                if($i==$giorno){
                                    echo '<option selected value="' . $i .'">'. $i . '</option>';
                                } else echo  '<option value="' . $i .'">'. $i . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Mese</label>
                        <select name="mese" class="uk-select">
                            <?php
                            echo $mese == 1 ? '<option selected value="01">Gennaio</option>' : '<option value="01">Gennaio</option>';
                            echo $mese == 2 ? '<option selected value="02">Febbraio</option>' : '<option value="02">Febbraio</option>';
                            echo $mese == 3 ? '<option selected value="03">Marzo</option>' : '<option value="03">Marzo</option>';
                            echo $mese == 4 ? '<option selected value="04">Aprile</option>' : '<option value="04">Aprile</option>';
                            echo $mese == 5 ? '<option selected value="05">Maggio</option>' : '<option value="05">Maggio</option>';
                            echo $mese == 6 ? '<option selected value="06">Giugno</option>' : '<option value="06">Giugno</option>';
                            echo $mese == 7 ? '<option selected value="07">Luglio</option>' : '<option value="07">Luglio</option>';
                            echo $mese == 8 ? '<option selected value="08">Agosto</option>' : '<option value="08">Agosto</option>';
                            echo $mese == 9 ? '<option selected value="09">Settembre</option>' : '<option value="09">Settembre</option>';
                            echo $mese == 10 ? '<option selected value="10">Ottobre</option>' : '<option value="10">Ottobre</option>';
                            echo $mese == 11 ? '<option selected value="11">Novembre</option>' : '<option value="11">Novembre</option>';
                            echo $mese == 12 ? '<option selected value="12">Dicembre</option>' : '<option value="12">Dicembre</option>';

                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Anno</label>
                        <select class="uk-select" type="text" name="anno" disabled>
                            <option value="<?=$anno?>"><?=$anno?></option>
                        </select>
                    </div>
                            */
                    ?> 
                    <div>
                        <label>Data (gg/mm/aaaa)</label>
                        <input type="text" class="uk-input" name="data" value="<?=$data_leggibile?>" />
                        </div>





                    
                    <div>
                        <label>Orario inizio</label>
                        <select class="uk-select" type="text" name="orario_inizio">
                            <?php
                            $inizio = 8;
                            $fine = 17;
                                for($i=$inizio; $i<=$fine; $i++){
                                    if ($i < 10)  $orario = '0' . $i . ':00' ; else  $orario =  $i . ':00';

                                    echo $orario_inizio == $orario ? '<option selected value="' . $orario . '">'. $orario . '</option>' : '<option value="' . $orario . '">'. $orario . '</option>'; 
                                
                                    if ($i < 10)  $orario = '0' . $i . ':30' ; else  $orario =  $i . ':30';
                                    echo $orario_inizio == $orario ? '<option selected value="' . $orario . '">'. $orario . '</option>' : '<option value="' . $orario . '">'. $orario . '</option>'; 
                                }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Orario fine</label>
                        <select class="uk-select" type="text" name="orario_fine">
                            <?php
                            $orario = '8:30' ;
                            echo $orario_fine == $orario ? '<option selected value="' . $orario . '">'. $orario . '</option>' : '<option value="' . $orario . '">'. $orario . '</option>'; 

                            $inizio = 9;

                            $fine = 18;

                                for($i=$inizio; $i<=$fine; $i++){
                                    if ($i < 10)  $orario = '0' . $i . ':30' ; else  $orario =  $i . ':30';
                                    echo $orario_fine == $orario ? '<option selected value="' . $orario . '">'. $orario . '</option>' : '<option value="' . $orario . '">'. $orario . '</option>'; 
                                
                                    if ($i < 10)  $orario = '0' . $i . ':00' ; else  $orario =  $i . ':00';
                                    echo $orario_fine == $orario ? '<option selected value="' . $orario . '">'. $orario . '</option>' : '<option value="' . $orario . '">'. $orario . '</option>'; 
                                }
                            ?>
                        </select>
                    </div>

                </div>


                <div>
                   <input type="submit" value="Verifica disponibilità" class="uk-button uk-button-primary" />
                </div>

            </div>
            <input type="hidden" name="nome" value="<?=$biblioteca["Nome"]?>"/>
        </form>





        <div id="elenco-posti-disponibili" uk-grid="masonry: false" class="uk-grid-small uk-flex-around ">
            <?php 

           // $data = "2021-03-20";

           // $orario_inizio = "08:30";
           // $orario_fine = "18:00";

            $query = $db->prepare("CALL VISUALIZZA_POSTI_LETTURA(:nome, :data, :orainizio, :orafine);");
           $query->execute(array(
                ":nome" => $biblioteca["Nome"],
                ":data" => date_format($data, 'Y-m-d'),
                ":orainizio" => $orario_inizio,
                ":orafine" => $orario_fine
            ));

                   while($posto = $query->fetch(PDO::FETCH_ASSOC)){ ?>

            <div>
                
            <?php if (!$posto["Prenotato"]) { ?>
            <a href="prenotazione-lettura-conferma.php?biblioteca=<?=urlencode($biblioteca["Nome"])?>&posto=<?=$posto["Numero"]?>&data=<?=urlencode($data->format('Y-m-d'))?>&orario_inizio=<?=urlencode($orario_inizio)?>&orario_fine=<?=urlencode($orario_fine)?>&ethernet=<?= $posto["PresaRete"] ? "1" : "0"?>&elettrico=<?= $posto["PresaCorrente"] ? "1" : "0"?>" class="uk-button uk-button-default">



                    Posto n. <?=$posto["Numero"]?>
                    <div uk-grid class="uk-flex-center uk-grid-small" style="margin-bottom: 10px; margin-top: -10px;">
                        <span uk-icon="bolt"
                            class="<?php echo  $posto["PresaRete"] ? 'uk-text-success' : 'uk-text-danger';?>"></span>
                        <span uk-icon="link"
                            class="<?php echo  $posto["PresaCorrente"] ? 'uk-text-success' : 'uk-text-danger';?>"></span>
                    </div>
            </a> 
            <?php } else { // già prenotato ?>
                <button disabled class="uk-button uk-button-default" style="cursor: not-allowed">


                Posto n. <?=$posto["Numero"]?>
                <div uk-grid class="uk-flex-center uk-grid-small" style="margin-bottom: 10px; margin-top: -10px;">
                    <span uk-icon="bolt"
                        class="uk-text-muted"></span>
                    <span uk-icon="link"
                        class="uk-text-muted"></span>
                </div>
            </button> 
            <?php } ?>

            </div>
            <?php
                    }

?>


        </div>

    </div>

    </div>
</body>

</html>
<?php
$db = null;
?>