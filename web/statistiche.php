<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';

?>

<head>
    <title>Statistiche</title>
    <?=get_head()?>


</head>

<body>
    <?php include 'template/header.php'; ?>
    <div class="uk-container uk-padding">
        <div uk-grid>
            <div class="uk-width-1-1">
                <div class="uk-card uk-card-default uk-card-body uk-margin">

                    <h1>Statistiche</h1>
                    <p> In questa pagina è possibile visualizzare alcune statistiche aggiornate in diretta sulla
                        piattaforma eBiblio</p>

                </div>
            </div>
            <div class="uk-width-1-2">
                <div class="uk-card uk-card-body uk-card-default">
                    <div class="uk-card-badge uk-card-badge-warning">LIVE <img
                            src="https://upload.wikimedia.org/wikipedia/commons/4/41/Red_circle.gif"
                            style="height:20px; margin-top: -2px;"></div>
                    <h3>Biblioteche libere</h3>
                    <p>Biblioteche con postazioni letture meno utilizzate attualmente</p>
                    <table class="uk-table uk-table-striped">
                        <thead>
                            <tr>
                                <th>Biblioteca</th>
                                <th>% posti liberi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        $query = $db->prepare('CALL STATISTICHE_BIBLIOTECHE_LIBERE();');
                        $query->execute();

                        while($row = $query->fetch(PDO::FETCH_ASSOC)){
                            echo ' <tr><td>' . $row["Nome"] . '</td><td>' . ($row["PercentualePosti"] * 100 ). ' %</td></tr>';
                        }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
            <div class="uk-width-1-2">
                <div class="uk-card uk-card-body uk-card-default">
                    <h3>Classifica volontari</h3>
                    <p>Classifica volontari che hanno effettuato più consegne (sia di affidamento che di ritiro di libri)</p>
                    <table class="uk-table uk-table-striped">
                        <thead>
                            <tr>
                                <th>Volontario</th>
                                <th>N. consegne </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $query = $db->prepare("CALL STATISTICHE_VOLONTARI();");
                        $query->execute();

                        while($volontario = $query->fetch(PDO::FETCH_ASSOC)){
                            echo '<tr><td>' . $volontario["Nome"] . " " . $volontario["Cognome"] . "</td><td>" . $volontario["NumeroConsegne"] . "</td></tr>";                        
                        }
                        ?> 
                        </tbody>

                    </table>
                </div>
            </div>
            <div class="uk-width-1-2">
                <div class="uk-card uk-card-body uk-card-default">
                    <h3>Classifica libri cartacei</h3>
                    <p>Classifica libri cartacei più prenotati</p>
                    <table class="uk-table uk-table-striped">
                        <thead>
                            <tr>
                                <th>Nome Libro</th>
                                <th>N. prenotazioni</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = $db->prepare('CALL STATISTICHE_CARTACEI();');
                            $query->execute();

                            while($row = $query->fetch(PDO::FETCH_ASSOC)){
                                echo ' <tr><td>' . $row["Titolo"] . '</td><td>' . $row["NumeroPrenotazioni"] .  ' </td></tr>';
                            }
                        ?>
                        </tbody>

                    </table>
                </div>
            </div>
            <div class="uk-width-1-2">
                <div class="uk-card uk-card-body uk-card-default">
                    <h3>Classifica E-book</h3>
                    <p>Classifica degli e-book più letti</p>
                    <table class="uk-table uk-table-striped">
                        <thead>
                            <tr>
                                <th>Nome E-book</th>
                                <th>N. accessi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                        $query = $db->prepare('CALL STATISTICHE_EBOOK();');
                        $query->execute();

                        while($row = $query->fetch(PDO::FETCH_ASSOC)){
                            echo ' <tr><td>' . $row["Titolo"] . '</td><td>' . $row["NumeroAccessi"] .  ' </td></tr>';
                        }
                        ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>