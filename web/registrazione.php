<?php


require 'utilities/functions.php';


if (isset($_POST["nome"])){
    // Inizio registrazione 
    //NomeIN VARCHAR(30), CognomeIN VARCHAR(30), EmailIN VARCHAR(30), PasswordIN VARCHAR(30), DataNascitaIN DATE, LuogoNascitaIN VARCHAR(30), TelefonoIN VARCHAR(15), ProfessioneIN VARCHAR(30))

    $data = date_create_from_format('d/m/Y', $_POST["data"]);
    if (!$data){
        header('Location: ?e=' . urlencode("La data inserita non è nel formato corretto"));
        die(0);
    }
    $data = date_format($data, 'Y-m-d');

    try {
        $query = $db->prepare("CALL REGISTRA_UTILIZZATORE(:nome, :cognome, :email, :password, :datanascita, :luogonascita, :telefono, :professione, :indirizzo, :citta, :cap )");
        $query->execute(array(
            ':nome' => $_POST["nome"],
            ':cognome' => $_POST["cognome"],
            ':email' => $_POST["email"],
            ':password' => password_hash($_POST["password"], PASSWORD_BCRYPT),
            ':datanascita' => $data,
            ':luogonascita' => $_POST["luogo"],
            ':telefono' => $_POST["telefono"],
            ':professione' => $_POST["professione"],
            ':indirizzo' => $_POST["indirizzo"],
            ':citta' => $_POST["citta"],
            ':cap' => $_POST["cap"]
        ));
        inviaLog("Codice Utente :".$_POST["email"]." - Operazione : Registrazione Utente " );

        header("Location: /login.php");
        die(0);

    } catch(Exception $e){
        //if($e->getCode() == 45000) 
        //    header('Location: ?e=' . urlencode("Esiste già un utente con quello username"));
        //else 
            header('Location: ?e=' . urlencode($e->getMessage()));
        die(0);
    }
  //  echo $query;
}


?>

<!DOCTYPE html>
<html>
<?php 
?>

<head>
    <?=get_head();?>
</head>

<body>
    <?php include 'template/header.php'; ?>

    <div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport" uk-height-viewport="offset-top: true">
        <!--<div class="uk-position-bottom-center uk-position-small uk-visible uk-text-center">
				<span class="uk-text-small uk-text-muted uk-text-center"><a href="/admin/attiva-utente/">Attiva credenziali</a><br>versione 1.0.0</span>
			</div>-->
        <div class="uk-width-xlarge uk-padding uk-background-muted">
            <form method="POST">
                <h4>Registrazione</h4>
                <fieldset class="uk-fieldset">
                    <!--<legend class="uk-legend">Login</legend>-->
                    <div class="uk-child-width-1-2" uk-grid>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="nome" required="" placeholder="Nome"
                                    type="text">
                            </div>
                        </div>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="cognome" required="" placeholder="Cognome"
                                    type="text">
                            </div>
                        </div>

                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="telefono" required="" placeholder="Telefono"
                                    type="text">
                            </div>
                        </div>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="data" required="" placeholder="Data nascita (gg/mm/aaaa)"
                                    type="text">
                            </div>
                        </div>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="luogo" required="" placeholder="Luogo nascita"
                                    type="text">
                            </div>
                        </div>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="professione" required="" placeholder="Professione"
                                    type="text">
                            </div>
                        </div>
                        <div class="uk-width-1-2">
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="indirizzo" required="" placeholder="Indirizzo"
                                    type="text">
                            </div>
                        </div>
                        <div class="uk-width-1-4">
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="cap" required="" placeholder="CAP"
                                    type="text">
                            </div>
                        </div>
                        <div class="uk-width-1-4">
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-input uk-form-large" name="citta" required="" placeholder="Città"
                                    type="text">
                            </div>
                        </div>


                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="user"></span>
                                <input class="uk-input uk-form-large" name="email" required="" placeholder="Email"
                                    type="text">
                            </div>
                        </div>
                        <div>
                            <div class="uk-inline uk-width-1-1">
                                <span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="lock"></span>
                                <input class="uk-input uk-form-large" name="password" required="" placeholder="Password"
                                    type="password">
                            </div>
                        </div>

                        <!--<div class="uk-margin">
							<label><input class="uk-checkbox" type="checkbox" name="keep"> Ricordami</label>
						</div>-->
                    </div>
                    <?php
						echo (isset($_GET["e"])) ? '<p class="uk-text-danger">'.$_GET["e"].'</p>' : "";
					?>
                    <div class="uk-margin">
                        <button type="submit"
                            class="uk-button uk-button-primary uk-button-primary uk-button-large uk-width-1-1">Registrati</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>


</body>

</html>