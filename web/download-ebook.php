<?php
require 'utilities/functions.php';

if(!isset($_SESSION["TipoUtente"]) || $_SESSION["TipoUtente"] != "Utilizzatore" ){
    header("Location: /login.php");
    die(0);
}

if(!isset($_GET["id"])){
    header("Location: /ricerca-libri.php");
    die(0);
}

$query = $db->prepare("CALL VISUALIZZA_EBOOK(:codicelibro, :codiceutilizzatore)");
$query->execute(array(
    ':codicelibro' => $_GET["id"],
    ':codiceutilizzatore' => $_SESSION["Codice"]
));

$libro = $query->fetch(PDO::FETCH_ASSOC);
header('Content-type: application/pdf');

readfile(substr($libro["PDF"], 1));


?>