<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';
$query = $db->prepare("CALL VISUALIZZA_LIBRO(:codice)", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
    $query->execute(array(
        ':codice' => $_GET["id"]
    ));
?>
<title>Libro</title>
<head>
    <?=get_head()?>
</head>

<body>
    <?php include 'template/header.php'; ?>


    <div class="uk-container uk-padding ">
        <div class="uk-expand uk-text-left"><button onclick="window.history.back();" class="uk-button uk-button"><span
                    uk-icon="arrow-left"></span> Torna indietro</button></div>
        <div class=" uk-card uk-card-default uk-card-body uk-margin">


            <?php
$libro = $query->fetchAll(PDO::FETCH_ASSOC)[0];
$query->closeCursor();

         echo  '<div uk-grid class="uk-flex-bottom">
                <div>
                    <h1>'.$libro["Titolo"].'</h1>
                </div>
                <div>
                    <h4> <span uk-icon="users"></span> '. listaAutori($db, $libro["Codice"]) .'</h4>
                </div>

            </div>

            <div>
                <p> <i>'.$libro["NomeEdizione"].' , '.$libro["Codice"].' </i></p>
            </div>

            <div uk-grid class="uk-flex-bottom">
                <div>
                    <h6><span uk-icon="calendar"></span> '.$libro["AnnoPubblicazione"].' </h6>
                </div>
                <div>
                    <h6><span uk-icon="bookmark"></span> '.$libro["Genere"].' </h6>
                </div>';
?>
            <?php 
            

            if($libro["Tipo"] == "Ebook") echo '<div><h6><span uk-icon="tablet"></span> E-Book</h6></div>';
            else if ($libro["Tipo"] == "Cartaceo") echo '<div><h6><span uk-icon="file-text"></span> Libro Cartaceo</h6></div>';
            if($libro["Tipo"] == "Cartaceo")
            if($libro["StatoPrestito"] == "Disponibile") echo '<div><h6><span uk-icon="check"></span> Disponibile</h6></div>';
            else echo '<div><h6><span uk-icon="close"></span> Non disponibile</h6></div>';
            if($libro["Tipo"] == "Cartaceo") echo ' <div><h6><span uk-icon="tag"></span> Stato : ' . $libro["StatoConservazione"] .  ' </h6></div>';
            if($libro["Tipo"] == "Cartaceo") echo  '<div><h6><span uk-icon="info"></span> Numero pagine: ' . $libro["NumeroPagine"] .  ' </h6></div>';
            if($libro["Tipo"] == "Cartaceo") echo  '<div><h6><span uk-icon="info"></span> Numero Scaffale: ' . $libro["NumeroScaffale"] .  ' </h6></div>';
            if($libro["Tipo"] == "Ebook") echo  '<div><h6><span uk-icon="file-pdf"></span> ' . $libro["Dimensione"] .  ' KB </h6></div>'
            ?>

        </div>

    </div>


    <?php
    if($libro["Tipo"] == 'Ebook') {
        ?>
    <div class="uk-margin-large uk-text-center" id="accesso-libro">
        <a href="/download-ebook.php?id=<?=$libro["Codice"]?>"
            class="uk-button uk-button-secondary uk-button-large">Scarica ora</a>
    </div>
    <?php
    }
    else if($libro["Tipo"] == "Cartaceo" && $libro["StatoPrestito"] == "Disponibile") {
        ?>
    <div class="uk-margin-large uk-text-center" id="accesso-libro">
        <a href="prenotazione-libro-conferma.php?id=<?=$libro["Codice"]?>"
            class="uk-button uk-button-secondary uk-button-large">Prenota ora</a>
    </div>
    <?php
    }

     if(isset($_SESSION["StatoAccount"]) && $_SESSION["StatoAccount"] == "Sospeso") echo '
    <div class="uk-alert uk-alert-danger"><h5>Account sospeso</h5><p>Il tuo account è stato sospeso per aver ricevuto 3 segnalazioni. <br>Non potrai più inserire prenotazioni. Puoi consultare le segnalazioni ricevute in <a href="/area-utente/elenco-segnalazioni.php">questa pagina</a>.</p></div><style>#accesso-libro {display:none}</style>';
    ?>


    </div>
</body>

</html>