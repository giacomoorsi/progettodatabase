<!DOCTYPE html>
<html>
<?php
    include '../utilities/functions.php';
    
        
    if(isset($_POST["messaggio"])){
       
        
        $query = $db->prepare("CALL INSERISCI_SEGNALAZIONE(:CodiceUtilizzatoreIN, :CodiceAmministratoreIN, :Testo);");
        $query->execute(array(
            ":CodiceUtilizzatoreIN"=>$_GET["idUtente"], 
            ":CodiceAmministratoreIN"=>$_SESSION["Codice"], 
            ":Testo"=> $_POST["messaggio"]
        ));
        
        inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione : Scrittura Segnalazione - id RiceventeNota : ".$_GET["idUtente"] );

        header("Location: elenco-segnalazioni-amministratore.php");

        
    }

?>



<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
    <title>Inserimento segnalazione</title>

</head>

<body>
    <?php include '../template/header.php' ?>
    <?php include '../template/left-bar.php' ?>



    <!-- Form per scrivere i messaggi/ nota -->
    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
    <div class="uk-card uk-card-default uk-card-body">
        <form class="uk-form" method="POST">
            <h1>Inserimento segnalazione</h1>
            <div class="uk-margin" uk-grid>
                <div class="uk-width-1-3">
                    <h5>Data</h5>
                    <input disabled type="text" class="uk-input uk-width-1-1" value="<?=date('d/m/Y')?>">
                </div>
            </div>
            <div class="uk-margin">
            <h5>Messaggio segnalazione</h5>
            <textarea class="uk-textarea uk-width-1-1"  rows="5" name="messaggio" ></textarea>
            </div>
            <div class="uk-margin">
                <input type="submit" value="Invia segnalazione" class="uk-button uk-button-primary">
            </div>
        </form>
        </div>
    </div>



</body>