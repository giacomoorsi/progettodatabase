<?php 
include '../utilities/functions.php';
if (isset($_POST["titolo"])){
 
    $query = $db->prepare("CALL INSERIMENTO_LIBRO_CARTACEO(:nomeBiblioteca, :titolo, :anno, :nomeedizione, :statoconservazione, :genere, :numeropagine, :numeroscaffale)");
    $query->execute(array(
        ':nomeBiblioteca' => $_SESSION["Biblioteca"],
        ':titolo' => $_POST["titolo"],
        ':anno' => $_POST["anno"],
        ':nomeedizione' => $_POST["nomeedizione"],
        ':statoconservazione' => $_POST["statoconservazione"],
        ':genere' => $_POST["genere"],
        ':numeropagine' => $_POST["numeropagine"],
        ':numeroscaffale' => $_POST["numeroscaffale"]
    ));
    inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Inserimento LibroCartaceo - Titolo LibroCartaceo : ".$_POST["titolo"] );

    //echo $query;
    $id = $query->fetch(PDO::FETCH_ASSOC);
    if(isset($_POST["autore"])){
        foreach($_POST["autore"] as $a){
            $query = $db->prepare("CALL INSERIMENTO_AUTORE_LIBRO(:idlibro, :idautore)");
            $query->execute(array(
                ':idlibro' => $id["ID"],
                ':idautore' => $a
            )); 
            inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Inserimento AutoreLibro - id Libro:  ".$id["ID"]." - id Autore : ".$a);

        }
    }
    header("Location: catalogo-biblioteca.php");


}

?>

<!DOCTYPE html>
<html>  
<?php 
?>
<title>Inserisci Libro</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php';
    include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Inserisci libro cartaceo</h1>

        <div class="uk-card uk-card-default uk-card-body">
            <h3>Informazioni libro</h3>

            <form method="POST" id="form">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Titolo</label>
                        <input class="uk-input uk-form-large" name="titolo" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Anno di pubblicazione</label>
                        <input class="uk-input uk-form-large" name="anno" required="" type="number">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Nome Edizione</label>
                        <input class="uk-input uk-form-large" name="nomeedizione" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Genere</label>
                        <input class="uk-input uk-form-large" name="genere" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Numero Pagine</label>
                        <input class="uk-input uk-form-large" name="numeropagine" required="" type="number">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Numero Scaffale</label>
                        <input class="uk-input uk-form-large" name="numeroscaffale" required="" type="number">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Stato Conservazione</label>
                        <select class="uk-input uk-select" name="statoconservazione">
                            <option value="Ottimo">Ottimo</option>
                            <option value="Buono">Buono</option>
                            <option value="Non Buono">Non Buono</option>
                            <option value="Scadente">Scadente</option>
                        </select>
                    </div>
                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button-primary" type="submit" value="Carica">Inserisci</button></div>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    <h4>Autore/i</h4>
                    <p>Premi su <span uk-icon="close"></span> per rimuovere l'autore. </p>
                    <ul class="uk-list" id="lista_autori">
                    </ul>
                    <a href="#autore" class="uk-button uk-button-text" uk-toggle>Aggiungi autore</a>
                </div>
            </form>



            <div id="autore" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <p>Scegli l'autore o <a href="aggiunta-autore.php">creane uno nuovo</a>.</p>
                    <table class="uk-table uk-table-divider">
                        <?php $query = $db->prepare("CALL LISTA_AUTORI()");
	                    $query->execute();

	                    while($autore = $query->fetch(PDO::FETCH_ASSOC)){        
                        echo '<tr><td>'.$autore['Nome']. "  " . $autore['Cognome'].'</td><td><a onclick="';
                        echo "aggiungiAutore('" . $autore['Nome'] . "', '" .  $autore['Cognome'] ."',". $autore['Codice'].")";
                        echo '" class="uk-icon-button" uk-icon="plus"></a></td></tr>';
                        }
                    ?>



                    </table>


                </div>
            </div>
        </div>
        <script>
            function aggiungiAutore(nome,cognome,id) {
                // deve andare ad aggiungere una riga nella lista degli autori sulla pagina
                //alert(nome);
                $('#lista_autori').append('<li><a onclick="rimuoviAutore(this, ' + id +
                    ')"><span uk-icon="close"></span></a>' + nome + " " + cognome +'</li>');
                $('#form').append('<input id="input-' + id + '" type="hidden" name="autore[]" value="' + id + '" />');
                UIkit.modal('#autore').hide();

            }

            function rimuoviAutore(element, id) {
                $(element).parent().remove();
                $('#input-' + id).remove();
            }
        </script>
</body>

</html>