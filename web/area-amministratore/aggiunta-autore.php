<?php 
include '../utilities/functions.php';
if (isset($_POST["nome"])){
    
    $query = $db->prepare("CALL INSERIMENTO_AUTORE(:nome,:cognome)");
    $query->execute(array(
        ':nome' => $_POST["nome"],
        ':cognome' => $_POST["cognome"]
    ));
    inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Inserimento Autore - Nome : ".$_POST["nome"]." - Cognome :".$_POST["cognome"] );
    header("Location: catalogo-biblioteca.php");
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Aggiunta autore</title>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php';
    $tipo = "Cartaceo";
    $button1 = "Conferma";
    $button2 = "Inserisci";
    include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Aggiungi Autore</h1>

        <div class="uk-card uk-card-default uk-card-body">

        <form method="POST">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Nome</label>
                        <input type="text" name = "nome"class=" uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-2">
                        <label>Cognome</label>
                        <input type="text" name = "cognome" class="uk-input uk-width-1-1">
                    </div>

                </div>
                <input class="uk-margin uk-button uk-button-primary" type="submit" value="Aggiungi" />


        </div>
        </form>
</body>

</html>