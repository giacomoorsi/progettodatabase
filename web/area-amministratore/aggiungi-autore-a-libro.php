<?php
/**
 * Aggiunge l'autore contenuto in $_GET["idautore"] al libro $_GET["idlibro"]
 */
require '../utilities/functions.php';

if(!isset($_GET["idlibro"]) || !isset($_GET["idautore"]) || !isset($_SESSION["Codice"]) || ($_SESSION["TipoUtente"]) != "Amministratore"){
    die(0);
}

$query = $db->prepare("CALL INSERIMENTO_AUTORE_LIBRO(:idLibro, :idAutore);");
try{
    $query->execute(array(
        ":idLibro" => $_GET["idlibro"],
        ":idAutore" => $_GET["idautore"]
    ));
} catch(PDOException $e){
    echo '<p>Errore. Non è possibile inserire più volte lo stesso autore</p>';
}

if(isset($_GET["cartaceo"])){
    header("Location: modifica-libro-cartaceo.php?id=" . $_GET["idlibro"]);
} else {
    header("Location: modifica-libro-ebook.php?id=" . $_GET["idlibro"]);
}
