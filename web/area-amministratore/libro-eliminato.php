<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
    <title>Libro Eliminato!</title>

</head>

<?php
    $query = $db->prepare("CALL ELIMINA_LIBRO(:codice)");
    $query->execute(array(
    ':codice' => $_GET["id"]
    ));
    inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Eliminazione Libro  - Codice Libro :". $_GET["id"] );
?>

<body>
<?php include '../template/header.php'; ?>
<?php include '../template/left-bar.php'; ?>
<div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
<div style="width:800px; margin:0 auto;"><h2> Eliminazione  di  "<?=$_GET["titolo"]?>" avvenuta con successo! </h2></div>
</div>
</body>