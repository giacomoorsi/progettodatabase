<!DOCTYPE html>
<html>
<?php 
require '../utilities/functions.php';
?>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
    <title>Catalogo biblioteca</title>

</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        
        <div class="uk-margin" uk-grid>
            <div><h1>Catalogo biblioteca</h1></div>
            <div class="uk-width-expand uk-text-right"><a href="inserimento_cartaceo.php" class="uk-button uk-button-secondary">Aggiungi cartaceo <span uk-icon="icon:plus; ratio:0.5"></span></a></div>
            <div class="uk-width-expand uk-text-right"><a href="inserimento_e-book.php" class="uk-button uk-button-secondary">Aggiungi e-book <span uk-icon="icon:plus; ratio:0.5"></span></a></div>
        </div>
        <div class="uk-card uk-card-default uk-card-body">
            <h3>Ricerca libro</h3>

            <form action="catalogo-biblioteca.php" method="POST">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Titolo</label>
                        <input name="titolo" class="uk-input uk-width-1-1" value="<?= isset($_POST["titolo"]) ? $_POST["titolo"] : ""; ?>">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Autore</label>
                        <input class="uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Anno di pubblicazione</label>
                        <input name="anno" type="number" class="uk-input uk-width-1-1" value="<?= isset($_POST["anno"]) ? $_POST["anno"] : ""; ?>">
                    </div>
                    <div class="uk-width-1-4">
                      <label>Tipo libro</label>
                      <select name="tipo" class="uk-input uk-select">
                        <option value="Qualsiasi">Qualsiasi</option>
                        <option value="Ebook" <?= (isset($_POST["tipo"]) && $_POST["tipo"]=="Ebook") ? "selected" : ""; ?>>E-book</option>
                        <option value="Cartaceo" <?= (isset($_POST["tipo"]) && $_POST["tipo"]=="Cartaceo") ? "selected" : ""; ?>>Libro Cartaceo</option>
                      </select>
                  </div>
                    <div class="uk-width-1-4">
                        <label>Codice</label>
                        <input name="codice" type="number" class="uk-input uk-width-1-1" value="<?= isset($_POST["codice"]) ? $_POST["codice"] : ""; ?>">
                    </div>
                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button-primary" type="submit" value="Ricerca">Ricerca</button>
                    </div>
                </div>
            </form>
        </div>
        
        <?php

        if(isset($_POST["titolo"])){
            $titolo = $_POST["titolo"];
        } else $titolo = "";

        if(isset($_POST["autore"])){
            $autore = $_POST["autore"];
        } else $autore = "";

        if(isset($_POST["codice"])){
            $codice = $_POST["codice"];
        } else $codice = 0;

        if(isset($_POST["anno"])){
            $anno = $_POST["anno"];
        } else $anno = 0;
        
        if(isset($_POST["tipo"])){
            if($_POST["tipo"] == "Qualsiasi"){
                $tipo = "";
            } else $tipo = $_POST["tipo"];
        } else $tipo = ""; 
        
        $query = $db->prepare("CALL FILTRA_LIBRI(:codLibro, :titolo, :autore, :annoPubbl, :tipoLibro, :biblioteca);");
	    
        
        $query->execute(array(
            ':codLibro' => $codice,
            ':titolo' => $titolo,
            ':autore' => $autore,
            ':annoPubbl' => $anno,
            ':tipoLibro' => $tipo,
            ':biblioteca' => $_SESSION["Biblioteca"]
        ));
        $libri = $query->fetchAll(PDO::FETCH_ASSOC);
        $query->closeCursor();

        $presente = false;
                
        foreach($libri as $libro){

       echo '<div class="uk-card uk-margin uk-card-default uk-card-body">
            <div class="uk-card-badge uk-label">#'.$libro["Codice"].'</div>
            <div uk-grid>
                <div class="uk-width-expand">
                    <h4 class="uk-text-muted uk-margin-remove">'.listaAutori($db, $libro["Codice"]).'</h4>
                    <h3 class="uk-margin-remove">'.$libro["Titolo"].'</h3>
                </div>
                <div class="uk-width-auto uk-text-right">
                </div>
            </div>
            <div class="uk-grid uk-margin-small">';
            if($libro["Tipo"] == "Cartaceo") echo '<div><span uk-icon="info"> </span>'.$libro["StatoPrestito"].'</div>';
                echo '<div><span uk-icon="file-text"></span>'.$libro["Tipo"].'</div>';
            if($libro["Tipo"] == "Cartaceo") echo  '<div class="uk-expand uk-text-right"><a href="modifica-libro-cartaceo.php?id='.$libro["Codice"].'" class="uk-button uk-button-text">Modifica scheda</a></div>';
            if($libro["Tipo"] == "Ebook") echo  '<div class="uk-expand uk-text-right"><a href="modifica-libro-ebook.php?id='.$libro["Codice"].'" class="uk-button uk-button-text">Modifica scheda</a></div>';
            echo '</div>
        </div>';
        }

        ?>

    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.catalogo-biblioteca>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
