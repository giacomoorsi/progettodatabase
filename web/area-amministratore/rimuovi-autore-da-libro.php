<?php
require '../utilities/functions.php';

if(!isset($_GET["idlibro"]) || !isset($_GET["idautore"]) || !isset($_SESSION["Codice"]) || ($_SESSION["TipoUtente"]) != "Amministratore"){
    header("Location: /area-amministratore/catalogo-biblioteca.php");
    die(0);
}

$query = $db->prepare("CALL RIMUOVI_AUTORE(:idLibro, :idAutore);");
$query->execute(array(
    ":idLibro" => $_GET["idlibro"],
    ":idAutore" => $_GET["idautore"]
));
if(isset($_GET["cartaceo"])){
    header("Location: modifica-libro-cartaceo.php?id=" . $_GET["idlibro"]);
} else {
    header("Location: modifica-libro-ebook.php?id=" . $_GET["idlibro"]);
}
