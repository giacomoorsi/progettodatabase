<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
if (isset($_POST["titolo"])){
$query = $db->prepare("CALL MODIFICA_LIBRO_EBOOK(:codice,:nomeBiblioteca, :titolo, :anno, :nomeedizione,:genere);");
$query->execute(array(
    ':codice' => $_GET["id"],
    ':nomeBiblioteca' => $_SESSION["Biblioteca"],
    ':titolo' => $_POST["titolo"],
    ':anno' => $_POST["anno"],
    ':nomeedizione' => $_POST["nomeedizione"],
    ':genere' => $_POST["genere"]
));
inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione : Modifica EBook  - id EBook : ".$_GET["id"] );

}


$query = $db->prepare("CALL VISUALIZZA_LIBRO(:codice)");
    $query->execute(array(
        ':codice' => $_GET["id"]
    ));
?>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
    <title>Modifica libro</title>

</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Libro <span class="uk-text-muted">#1732</span></h1>

        <div class="uk-card uk-card-default uk-card-body">
            <h3>Modifica informazioni</h3>

            <form method="POST">
            <?php
            $libro = $query->fetch(PDO::FETCH_ASSOC);
               echo ' <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-3-4">
                        <label>Titolo</label>
                        <input class="uk-input uk-width-1-1" value="'.$libro["Titolo"].'" name="titolo">
                    </div>

                    <div class="uk-width-1-4">
                        <label>Anno di pubblicazione</label>
                        <input type="number" class="uk-input uk-width-1-1" value="'.$libro["AnnoPubblicazione"].'" name="anno">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Genere</label>
                        <input class="uk-input uk-width-1-1" value="'.$libro["Genere"].'" name="genere">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Edizione</label>
                        <input class="uk-input uk-width-1-1" value="'.$libro["NomeEdizione"].'" name="nomeedizione">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Tipo libro</label>
                        <select disabled class="uk-input uk-select">
                            <option selected>'.$libro["Tipo"].'</option> 
                        </select>
                    </div>';
                    ?>
                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button uk-button-" type="submit" value="Ricerca">Salva modifiche</button>
                    </div>
                </div>
            </form>

        </div>
        <div class="uk-margin">
            <a href="libro-eliminato.php?id=<?=$libro["Codice"]?>&titolo=<?=$libro["Titolo"]?>" class="uk-button uk-button-text">Elimina libro</a>
        </div>

        <div class="uk-card uk-card-default uk-card-body">
            <h4>Modifica autore/i</h4>
            <p>Premi su <span uk-icon="close"></span> per rimuvuovere l'autore. </p>
            <ul class="uk-list">
            <?php
                $query = $db->prepare("CALL VISUALIZZA_AUTORI(:codiceLibro)");
                $query->execute(array(
                    ':codiceLibro'=>$_GET["id"]
                ));
                
                while($autore = $query->fetch(PDO::FETCH_ASSOC)){ 
                    echo '<li><a href="rimuovi-autore-da-libro.php?ebook=1&idlibro='.$_GET["id"] . '&idautore='.$autore["Codice"].'">';
                    echo '<span uk-icon="close"></span></a>'. $autore["Nome"] . " " . $autore["Cognome"].  '</li>';
                }
                ?>
            </ul>
            <a href="#autore" class="uk-button uk-button-text" uk-toggle>Aggiungi autore</a>

            <div id="autore" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <p>Scegli l'autore o <a href="aggiunta-autore.php">creane uno nuovo</a>.</p>
                    <table class="uk-table uk-table-divider">
                        <?php $query = $db->prepare("CALL LISTA_AUTORI()");
	                    $query->execute();

	                    while($autore = $query->fetch(PDO::FETCH_ASSOC)){        
                        echo '<tr><td>'.$autore['Nome']. "  " . $autore['Cognome'].'</td><td><a href="aggiungi-autore-a-libro.php?ebook=1&idautore='.$autore["Codice"].'&idlibro='.$_GET["id"].' class="uk-icon-button" uk-icon="plus"></a></td></tr>';
                        }
                    ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.catalogo-biblioteca>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>
