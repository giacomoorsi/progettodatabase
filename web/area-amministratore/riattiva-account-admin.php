<!DOCTYPE html>

<?php 
include '../utilities/functions.php';

if(isset($_GET["idUtente"])){
    $id = $_GET["idUtente"];
    $query = $db->prepare("CALL RIMUOVI_SEGNALAZIONI(:CodiceUtilizzatoreIN)");
    $query ->execute(array(
        ':CodiceUtilizzatoreIN'=> $id
    ));
    inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione : Rimozione Segnalazioni  - id Utente : ".$id );

}
?>
<title>Elenco segnalazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
       
        <div class="uk-margin" uk-grid>
            <div><h1>Elenco utenti Sospesi</h1></div>
        </div>

        <?php
            $query = $db->prepare("CALL VISUALIZZA_ACCOUNT_BLOCCATI()");
            $query->execute();
            $mostra = false;
            while($user = $query->fetch(PDO::FETCH_ASSOC)){   
                $mostra = true;
                
        ?>
        <div class="uk-card uk-margin uk-card-default uk-card-body">
            <h3 class="uk-margin-remove uk-text-danger"><?=$user["Nome"]." ".$user["Cognome"]?></h3>
            <p>Codice utente : <?=" ".$user["Codice"]?> </p>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                
                <div class="uk-expand uk-text-right">
                    <a href="riattiva-account-admin.php?idUtente=<?=$user["Codice"]?>" class="uk-button uk-button-text" >
                        Elimina tutte le segnalazioni
                    </a>
                </div>
                
            </div>
        </div>

            <?php }
             if(!$mostra){ ?>
                <div class="uk-card uk-margin uk-card-default uk-card-body">
                     <h3 class="uk-margin-remove uk-text">Nessun utente è stato Sospeso!</h3>
                </div>
        

            <?php }
        ?>



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-segnalazioni>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>

</html>
