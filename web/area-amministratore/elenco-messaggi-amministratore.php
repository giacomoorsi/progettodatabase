<!DOCTYPE html>
<html>
<?php
include '../utilities/functions.php';
?>

<head>
    <title>Elenco messaggi</title>
    <?= get_head(); ?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco messaggi inviati</h1>
        <?php
            $query = $db->prepare("CALL VISUALIZZA_MESSAGGI_ADMIN(:codiceAmministratore)");
            $query->execute(array(
                ':codiceAmministratore' => $_SESSION["Codice"]
            ));

            while($msg =$query->fetch(PDO::FETCH_ASSOC)){   
                
        ?>
            <div class="uk-card uk-card-default uk-card-body">
                <h3 class="uk-margin-remove"><?=$msg["Titolo"]?></h3>
                <p><?=$msg["Testo"]?></p>
                <div class="uk-grid-small uk-margin-small" uk-grid>
                    <div><span uk-icon="calendar"></span><?= date_format(date_create_from_format('Y-m-d',$msg["Dataa"]), 'd/m/Y')?></div>
                    <div><span uk-icon="user"></span><?=$msg["Nome"]."  ".$msg["Cognome"]?></div>
                </div>
            </div>;
            <?php }
        ?>

    
    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-messaggi>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>

</html>