<?php 
include '../utilities/functions.php';
if (isset($_POST["titolo"]) && isset($_FILES["file"]["tmp_name"]) && $_FILES["file"]["type"]=="application/pdf"){
    
    $query = $db->prepare("CALL INSERIMENTO_LIBRO_EBOOK(:nomeBiblioteca, :titolo, :anno, :nomeedizione,:PDF,:dimensione, :genere);");
    $query->execute(array(
        ':nomeBiblioteca' => $_SESSION["Biblioteca"],
        ':titolo' => $_POST["titolo"],
        ':anno' => $_POST["anno"],
        ':nomeedizione' => $_POST["nomeedizione"],
        ':PDF' => "",
        ':dimensione' => 0,
        ':genere' => $_POST["genere"]
    ));
    inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione : Inserimento EBook - Titolo EBook : ".$_POST["titolo"] );

    //echo $query;
    $id = $query->fetch(PDO::FETCH_ASSOC);
    $id =  $id["ID"];
    if(isset($_POST["autore"])){
        foreach($_POST["autore"] as $a){
            $query = $db->prepare("CALL INSERIMENTO_AUTORE_LIBRO(:idlibro, :idautore);");
            $query->execute(array(
                ':idlibro' => $id,
                ':idautore' => $a
            )); 
            inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Inserimento Autore EBook - id EBook : ".$id." - id Autore".$a );

        }
    }

    // Upload

    $target_dir = "/ebooks/";
    $target_file = $target_dir . $id . '.pdf';

    if (move_uploaded_file($_FILES["file"]["tmp_name"], '..'. $target_file)){
        // upload completato
        $query = $db->prepare("CALL AGGIUNGI_PDF_EBOOK (:codice, :path, :dimensione);");
        $query->execute(array(
            ':codice' => $id,
            ':path' => $target_file,
            ':dimensione' => ($_FILES["file"]["size"] / 1000)
        ));
        inviaLog("Codice Admin :".$_SESSION["Codice"]." - Operazione :Inserimento PDF EBook - Codice EBook : ".$id );

        header("Location: catalogo-biblioteca.php");

    } else {
        echo '<script>alert("Errore nel caricamento del file.");</script>';
    }




} if(isset($_FILES["file"]) && $_FILES["file"]["type"]!="application/pdf"){
    echo '<script>alert("Errore! Il file deve essere un pdf");</script>';
}

?>

<!DOCTYPE html>
<html>
<?php 
?>
<title>Inserisci Libro</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php';
    include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Inserisci e-book</h1>

        <div class="uk-card uk-card-default uk-card-body">
            <h3>Informazioni libro</h3>

            <form method="POST" id="form" enctype="multipart/form-data">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Titolo</label>
                        <input class="uk-input uk-form-large" name="titolo" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Anno di pubblicazione</label>
                        <input class="uk-input uk-form-large" name="anno" required="" type="number">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Nome Edizione</label>
                        <input class="uk-input uk-form-large" name="nomeedizione" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>Genere</label>
                        <input class="uk-input uk-form-large" name="genere" required="" type="text">
                    </div>
                    <div class="uk-width-1-4">
                        <label>File (pdf)</label>
                        <input name="file" type="file" class="uk-input" required="">
                    </div>


                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button-primary" type="submit" value="Carica">Carica</button></div>
                </div>
                <div class="uk-card uk-card-default uk-card-body">
                    <h4>Autore/i</h4>
                    <p>Premi su <span uk-icon="close"></span> per rimuovere l'autore. </p>
                    <ul class="uk-list" id="lista_autori">
                    </ul>
                    <a href="#autore" class="uk-button uk-button-text" uk-toggle>Aggiungi autore</a>
                </div>
            </form>



            <div id="autore" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <p>Scegli l'autore o <a href="aggiunta-autore.php">creane uno nuovo</a>.</p>
                    <table class="uk-table uk-table-divider">
                        <?php $query = $db->prepare("CALL LISTA_AUTORI()");
	                    $query->execute();

	                    while($autore = $query->fetch(PDO::FETCH_ASSOC)){        
                        echo '<tr><td>'.$autore['Nome']. "  " . $autore['Cognome'].'</td><td><a onclick="';
                        echo "aggiungiAutore('" . $autore['Nome'] . "', '" .  $autore['Cognome'] ."',". $autore['Codice'].")";
                        echo '" class="uk-icon-button" uk-icon="plus"></a></td></tr>';
                        }
                    ?>



                    </table>

                </div>
            </div>
        </div>
        <script>
            function aggiungiAutore(nome,cognome,id) {
                // deve andare ad aggiungere una riga nella lista degli autori sulla pagina
                //alert(nome);
                $('#lista_autori').append('<li><a onclick="rimuoviAutore(this, ' + id +
                    ')"><span uk-icon="close"></span></a>' + nome + " " + cognome +'</li>');
                $('#form').append('<input id="input-' + id + '" type="hidden" name="autore[]" value="' + id + '" />');
                UIkit.modal('#autore').hide();

            }

            function rimuoviAutore(element, id) {
                $(element).parent().remove();
                $('#input-' + id).remove();
            }
        </script>
</body>

</html>