<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';

if(isset($_POST["codiceUt"])){
    $codiceUt = $_POST["codiceUt"];
}else {
    $codiceUt = 0;
}

if(isset($_POST["codiceLib"])){
    $codiceLib = $_POST["codiceLib"];
}else {
    $codiceLib = 0;
}

if(isset($_POST["dataI"])){
    $dataI = date_create_from_format('d/m/Y', $_POST["dataI"] );
}else {
    $dataI = date_create_from_format( 'd/m/Y', "01/01/2000"); ;
}

if(isset($_POST["codiceLib"])){
    $dataF =  date_create_from_format('d/m/Y', $_POST["dataF"] );
}else {
    $dataF = date_create_from_format( 'd/m/Y',"01/01/2050");
}

$query = $db->prepare("CALL FILTRA_PRENOTAZIONE( :dataI, :dataF, :codiceLib, :codiceUt, :biblioteca);");
$query->execute(array(
    ':codiceUt' => $codiceUt,
    ':codiceLib' =>$codiceLib,
    ':dataI' => date_format($dataI, "Y-m-d"),
    ':dataF' => date_format($dataF, "Y-m-d"),
    ':biblioteca' => $_SESSION["Biblioteca"]
));

?>
<title>Elenco prenotazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
        <h1>Elenco prenotazioni</h1>

        <div class="uk-card uk-card-default uk-card-body ">
            <h3>Filtra prenotazioni</h3>
            <form action="elenco-prenotazioni-biblioteca.php" method="POST">
                <div uk-grid class="uk-flex-bottom">
                    <div class="uk-width-1-2">
                        <label>Codice libro</label>
                        <input name="codiceLib" type="number" class="uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-2">
                        <label>Codice utilizzatore</label>
                        <input name="codiceUt"type="number" class="uk-input uk-width-1-1">
                    </div>
                    <div class="uk-width-1-2">
                        <label>Data inizio filtro</label>
                        <input name="dataI" type="text" class="uk-input uk-width-1-1" placeholder="gg/mm/aaaa" required  >
                    </div>
                    <div class="uk-width-1-2">
                        <label>Data fine filtro</label>
                        <input name="dataF"type="text" class="uk-input uk-width-1-1" placeholder="gg/mm/aaaa" required >
                    </div>
                    <div class="uk-width-1-4 uk-width-expand uk-text-right uk-valign-bottom">
                        <button class="uk-button uk-button uk-button-" type="submit" value="Ricerca">Ricerca</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- 
            parto php in cui stampo le prenotazioni che sono dalla query 
         -->
        <?php
         $presente = false;
         
       
            try {
                while($prenotazioniFiltrate = $query->fetch(PDO::FETCH_ASSOC)) {
                    $presente = true;
                    
                   ?>

                    <div class="uk-card uk-margin uk-card-default uk-card-body">
                        <div class="uk-card-badge  uk-label">Prenotazione n. <?=$prenotazioniFiltrate["Codice"]?></div>
                        <div uk-grid>
                            <div class="uk-width-expand">
                                <!-- <h4 class="uk-text-muted uk-margin-remove"><?=$prenotazioniFiltrate["Nome"]?>  <?=$prenotazioniFiltrate["Cognome"]?></h4> -->
                                <h3 class="uk-margin-remove"><?=$prenotazioniFiltrate["Titolo"]?></h3>
                            </div>
                            <div class="uk-width-auto uk-text-right">
                            </div>
                        </div>
                        <div class="uk-grid uk-margin-small">
                            <div><span uk-icon="user"> </span> <?=$prenotazioniFiltrate["NomeU"]?>  <?=$prenotazioniFiltrate["CognomeU"]?></div>
                            <?php if ($prenotazioniFiltrate["DataInizio"] != "") { ?>
                            <div><span uk-icon="calendar"></span> dal <?=date_format(date_create_from_format('Y-m-d',$prenotazioniFiltrate["DataInizio"]), 'd/m/Y')?> al <?=date_format(date_create_from_format('Y-m-d',$prenotazioniFiltrate["DataFine"]), 'd/m/Y')?></div>
                           <?php } ?>
                            <div class="uk-expand uk-text-right"><a href="riepilogo-prenotazione.php?idPrenotazione=<?=$prenotazioniFiltrate["Codice"]?>&NomeU=<?=$prenotazioniFiltrate["NomeU"]?>&CognomeU=<?=$prenotazioniFiltrate["CognomeU"]?>&idUtente=<?=$prenotazioniFiltrate["Codice"]?>" class="uk-button uk-button-text">Apri prenotazione</a></div>
                        </div>
                    </div>
                    <?php }
            
                    if(($presente == false) ){ ?>
                        <script> UIkit.notification('Nessuna prenotazione è stata effettuata... ', 'warning', 'bottom-center');</script>
                     <?php }
            } catch (Throwable  $e) { ?>
             <script>  UIkit.notification('Non è stata compilata correttamente il form per la ricerca... ', 'warning', 'bottom-center');</script> 
                <?php }
        ?>
    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-prenotazioni>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }

</style>

</html>