<!DOCTYPE html>
<html>
<?php 
include '../utilities/functions.php';
?>
<title>Elenco segnalazioni</title>

<head>
    <?=get_head();?>
    <link rel="stylesheet" type="text/css" href="/template/dashboard.css">
</head>

<body>
    <?php include '../template/header.php'; ?>
    <?php include '../template/left-bar.php'; ?>


    <div id="content" data-uk-height-viewport="expand: true" class="uk-container uk-container-large uk-padding-large">
       
        <div class="uk-margin" uk-grid>
            <div><h1>Elenco segnalazioni</h1></div>
            <div class="uk-width-expand uk-text-right"><a href="riattiva-account-admin.php" class="uk-button uk-button-secondary">Riattiva Utente <span uk-icon="icon:user; ratio:0.5"></span></a></div>
        </div>

        <?php
            $query = $db->prepare("CALL VISUALIZZA_SEGNALAZIONI_ADMIN(:codiceAmministratore)");
            $query->execute(array(
                ':codiceAmministratore' => $_SESSION["Codice"]
            ));
            $mostra = false;
            while($segnalazione =$query->fetch(PDO::FETCH_ASSOC)){   
                $mostra = true;
        ?>
        <div class="uk-card uk-margin uk-card-default uk-card-body">
            <h3 class="uk-margin-remove uk-text-danger">Segnalazione</h3>
            <p><?=$segnalazione["Testo"]?></p>
            <div class="uk-grid-small uk-margin-small" uk-grid>
                <div><span uk-icon="calendar"></span><?=date_format(date_create_from_format('Y-m-d',$segnalazione["Dataa"]), 'd/m/Y')?></div>
                <div><span uk-icon="user"></span><?=$segnalazione["Nome"]."  ".$segnalazione["Cognome"]?></div>
            </div>
        </div>
           
            <?php }
            if(!$mostra){ ?>
                <div class="uk-card uk-margin uk-card-default uk-card-body">
                     <h3 class="uk-margin-remove uk-text">Non è presente alcuna segnalazione!</h3>
                </div>
        

            <?php }
        ?>



    </div>
</body>

<style>
    /** selezione voce menu **/

    #left-col ul.uk-nav-default>li.elenco-segnalazioni>a {
        border-left: 2px solid #39f;
        padding-left: 30px;
        color: white;
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>

</html>