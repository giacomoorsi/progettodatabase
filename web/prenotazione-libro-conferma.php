<!DOCTYPE html>
<html>
<?php 
require 'utilities/functions.php';
if(!isset($_GET["id"]) || !isset($_SESSION["Codice"]) || $_SESSION["TipoUtente"] != "Utilizzatore"){
    header('Location: /login.php');
    die(0);
}


if(isset($_GET["conferma"])){
    $query = $db->prepare("CALL INSERISCI_PRENOTAZIONE(:codiceLibro, :codiceUtilizzatore);");
    $query->execute(array(
        ':codiceLibro' => $_GET["id"],
        ':codiceUtilizzatore' => $_SESSION["Codice"]
    ));
    inviaLog("Codice Utente :".$_SESSION["Codice"]." - Operazione :Prenotazione Libro - id Libro : ". $_GET["id"] );
    header("Location: /area-utente/");
    die(0);
}






$query = $db->prepare("CALL VISUALIZZA_LIBRO(:codice)");
    $query->execute(array(
        ':codice' => $_GET["id"]
    ));
?>

<head>
    <?=get_head()?>
</head>

<body>
    <?php include 'template/header.php';   

$libro = $query->fetch(PDO::FETCH_ASSOC);

echo '<div class="uk-container uk-section-large uk-container-small">
    <div class="uk-expand uk-text-left"><button onclick="window.history.back();" class="uk-button uk-button"><span uk-icon="arrow-left"></span> Torna indietro</button></div>
        <div class="uk-card uk-margin uk-card-default uk-card-body">
                    <div uk-grid class="uk-margin">
                        <div><h2 class="uk-margin-remove">'.$libro["Titolo"].'</h2></div>
                        <div class="uk-text-right uk-width-expand"><a class="uk-button uk-button-secondary uk-button-large" href="?id='. $_GET["id"] . '&conferma=1">Conferma</a></div>
                    </div>
                <div class="uk-grid uk-margin-small">
                    <div><span uk-icon="tag"> </span>'.$libro["StatoConservazione"].'</div>
                    <div><span uk-icon="location"></span>'.$libro["NomeBiblioteca"].'</div>
                </div>
                <p>La prenotazione verrà presa in carico da un volontario che ti consegnerà il libro a domicilio. La durata della prenotazione sarà di 15 giorni a partire dalla data di consegna. Un volontario verrà a ritirare il libro a domicilio al termine del periodo. 
        </div>
    </div> ';
?>

</body>

</html>